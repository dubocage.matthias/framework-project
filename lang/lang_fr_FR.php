<?php 

/** VIEW layout.php */
define('HTML_LAYOUT_LINK_MOVIE', 'Films');
define('HTML_LAYOUT_LINK_RANDOM', 'Aléatoire');
define('HTML_LAYOUT_LINK_MOVIESVIEW', 'Vos films vus');
define('HTML_LAYOUT_INPUT_LOGOUT', 'Deconnexion');
define('HTML_LAYOUT_INPUT_LOGIN', 'Connexion');

/** VIEW connection.php */
define('HTML_CONNECTION_TITLE_LOGIN', 'Connexion');
define('HTML_CONNECTION_LABEL_USERNAME', 'Nom d\'utilisateur');
define('HTML_CONNECTION_INPUT_PLACEHOLDER_USERNAME', 'Entrez votre nom d\'utilisateur');
define('HTML_CONNECTION_LABEL_PASSWORD', 'Mot de passe');
define('HTML_CONNECTION_INPUT_PLACEHOLDER_PASSWORD', 'Entrez votre mot de passe');
define('HTML_CONNECTION_INPUT_SUBMIT', 'Se connecter');
define('HTML_CONNECTION_INPUT_INSCRIPTION', 'S\'inscrire');

/** VIEW registration.php */
define('HTML_REGISTRATION_TITLE', 'Inscription');
define('HTML_REGISTRATION_INPUT_LABEL_USERNAME', 'Nom d\'utilisateur');
define('HTML_REGISTRATION_INPUT_PLACEHOLDER_USERNAME', 'Entrez votre nom d\'utilisateur');
define('HTML_REGISTRATION_INPUT_LABEL_MAIL', 'Adresse mail');
define('HTML_REGISTRATION_INPUT_PLACEHOLDER_MAIL', 'Entrez votre adresse mail');
define('HTML_REGISTRATION_INPUT_LABEL_PASSWORD', 'Mot de passe');
define('HTML_REGISTRATION_INPUT_PLACEHOLDER_PASSWORD', 'Entrez votre mot de passe');
define('HTML_REGISTRATION_INPUT_PLACEHOLDER_PASSWORDAGAIN', 'Entre le même mot de passe');
define('HTML_REGISTRATION_INPUT_SUBMIT', 'Valider');

/** VIEW movie/all.php */
define('HTML_MOVIE_ALL_TITLE', 'Les films');
define('HTML_MOVIE_ALL_INPUT_SUBMIT_SEARCH', 'Rechercher');
define('HTML_MOVIE_ALL_INPUT_PLACEHOLDER_SEARCH', 'Recherche');

/** VIEW movie/show.php */
define('HTML_MOVIE_SHOW_RELEASE_DATE', 'Date de sortie');
define('HTML_MOVIE_SHOW_DURATION', 'Durée');
define('HTML_MOVIE_SHOW_VIEW', 'Vus');
define('HTML_MOVIE_SHOW_VIEW_TIMES', 'fois');
define('HTML_MOVIE_SHOW_SYNOPSIS', 'Synopsis');

/** VIEW profile/view.php */
define('HTML_PROFILE_VIEW_TITLE', 'Les films vus de ');
define('HTML_PROFILE_VIEW_INPUT_SUBMIT_SEARCH', 'Rechercher');
define('HTML_PROFILE_VIEW_INPUT_PLACEHOLDER_SEARCH', 'Recherche');

/** VIEW template/listMovie.php */
define('HTML_TEMPLATE_MOVIELIST_INPUT_VIEW', 'Vus ');
define('HTML_TEMPLATE_MOVIELIST_INPUT_VIEW_TIMES', ' fois');
define('HTML_TEMPLATE_MOVIELIST_DURATION', 'Durée ');
define('HTML_TEMPLATE_MOVIELIST_SYNOPSIS', 'Synopsis ');
define('HTML_TEMPLATE_MOVIELIST_RELEASE_DATE', 'Date de sortie');

/**APP Validator.php */
define('VALIDATOR_ERROR_REQUIRED', '{$name} requis');
define('VALIDATOR_ERROR_USERNAME', 'Le pseudo est invalide: 5 caractères min/ 20 caractères max/ pas de caractères spéciaux');
define('VALIDATOR_ERROR_PASSWORD', 'Le mot de passe est invalide : 16 caractères minimum/ 32 caractères max/ doit contenire au moins une miniscule, majuscule, chiffre, caractère spicial(%#:$*!)');
define('VALIDATOR_ERROR_MAIL', 'Le mail est invalide');
define('VALIDATOR_ERROR_IDVALUE', '{$name} doit être un nombre positif!');

/** EXCEPTION */
define('EXCEPTION_NOTFOUNDEXCEPTION_MESSAGE','La page demandée est introuvable.');