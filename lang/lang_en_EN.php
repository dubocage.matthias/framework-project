<?php 

/** VIEW layout.php */
define('HTML_LAYOUT_LINK_MOVIE', 'Movies');
define('HTML_LAYOUT_LINK_RANDOM', 'Random');
define('HTML_LAYOUT_LINK_MOVIESVIEW', 'Your movies watch');
define('HTML_LAYOUT_INPUT_LOGOUT', 'Logout');
define('HTML_LAYOUT_INPUT_LOGIN', 'Login');

/** VIEW connection.php */
define('HTML_CONNECTION_TITLE_LOGIN', 'Login');
define('HTML_CONNECTION_LABEL_USERNAME', 'Username');
define('HTML_CONNECTION_INPUT_PLACEHOLDER_USERNAME', 'Enter your username');
define('HTML_CONNECTION_LABEL_PASSWORD', 'Password');
define('HTML_CONNECTION_INPUT_PLACEHOLDER_PASSWORD', 'Enter your password');
define('HTML_CONNECTION_INPUT_SUBMIT', 'Login');
define('HTML_CONNECTION_INPUT_INSCRIPTION', 'Register');

/** VIEW registration.php */
define('HTML_REGISTRATION_TITLE', 'Login');
define('HTML_REGISTRATION_INPUT_LABEL_USERNAME', 'Username');
define('HTML_REGISTRATION_INPUT_PLACEHOLDER_USERNAME', 'Enter your username');
define('HTML_REGISTRATION_INPUT_LABEL_MAIL', 'Mail address');
define('HTML_REGISTRATION_INPUT_PLACEHOLDER_MAIL', 'Enter your email address');
define('HTML_REGISTRATION_INPUT_LABEL_PASSWORD', 'Password');
define('HTML_REGISTRATION_INPUT_PLACEHOLDER_PASSWORD', 'Enter your password');
define('HTML_REGISTRATION_INPUT_PLACEHOLDER_PASSWORDAGAIN', 'Enter the same password');
define('HTML_REGISTRATION_INPUT_SUBMIT', 'Validate');

/** VIEW movie/all.php */
define('HTML_MOVIE_ALL_TITLE', 'Movies');
define('HTML_MOVIE_ALL_INPUT_SUBMIT_SEARCH', 'Search');
define('HTML_MOVIE_ALL_INPUT_PLACEHOLDER_SEARCH', 'Research');

/** VIEW movie/show.php */
define('HTML_MOVIE_SHOW_RELEASE_DATE', 'Release date');
define('HTML_MOVIE_SHOW_DURATION', 'Duration');
define('HTML_MOVIE_SHOW_VIEW', 'Watch');
define('HTML_MOVIE_SHOW_VIEW_TIMES', ' times');
define('HTML_MOVIE_SHOW_SYNOPSIS', 'Synopsis');

/** VIEW profile/view.php */
define('HTML_PROFILE_VIEW_TITLE', 'Movies watch by ');
define('HTML_PROFILE_VIEW_INPUT_SUBMIT_SEARCH', 'Search');
define('HTML_PROFILE_VIEW_INPUT_PLACEHOLDER_SEARCH', 'Research');

/** VIEW template/listMovie.php */
define('HTML_TEMPLATE_MOVIELIST_INPUT_VIEW', 'Watch ');
define('HTML_TEMPLATE_MOVIELIST_INPUT_VIEW_TIMES', ' times');
define('HTML_TEMPLATE_MOVIELIST_DURATION', 'Duration');
define('HTML_TEMPLATE_MOVIELIST_SYNOPSIS', 'Synopsis');
define('HTML_TEMPLATE_MOVIELIST_RELEASE_DATE', 'Release date');

/**APP Validator.php */
define('VALIDATOR_ERROR_REQUIRED', '{$name} requis');
define('VALIDATOR_ERROR_USERNAME', 'Le pseudo est invalide: 5 caractères min/ 20 caractères max/ pas de caractères spéciaux');
define('VALIDATOR_ERROR_PASSWORD', 'Le mot de passe est invalide : 16 caractères minimum/ 32 caractères max/ doit contenire au moins une miniscule, majuscule, chiffre, caractère spicial(%#:$*!)');
define('VALIDATOR_ERROR_MAIL', 'Le mail est invalide');
define('VALIDATOR_ERROR_IDVALUE', '{$name} doit être un nombre positif!');

/** EXCEPTION */
define('EXCEPTION_NOTFOUNDEXCEPTION_MESSAGE','The desired page can not be found.');