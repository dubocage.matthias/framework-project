<?php

namespace Logger;

use DateTime;

class Logger{

    public static function addLog(string $msg, string $level, string $ip)
    {
        $date = new DateTime('now');
        if($fp = fopen( dirname(__DIR__). DIRECTORY_SEPARATOR . "logger". DIRECTORY_SEPARATOR . "Logs" . DIRECTORY_SEPARATOR . $date->format('d_m_Y') . "_log.txt", "a+")) {
            fwrite($fp,"{".$level."}[".$date->format("H:i")."] ".$msg."[".$ip."]".PHP_EOL);
            fclose($fp);
        }
    }

}