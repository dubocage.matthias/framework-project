<?php

namespace Database;

use App\Business\DAOs\Interfaces\DBConnectionInterface;
use App\Business\DAOs\ArrayValue;
use App\Exceptions\NotCloneException;
use App\Exceptions\RequestDBException;
use Exception;
use PDO;

class DBConnection implements DBConnectionInterface 
{
    private const DEFAULT_SQL_USER = 'root';
    private const DEFAULT_SQL_HOST = '127.0.0.1';
    private const DEFAULT_SQL_PASS = '';
    private const DEFAULT_SQL_DTB = 'home_framework';

    private $pdo = null;
    private static $instance = null;

    /**
     * 
     */
    private function __construct()
    {
        $this->pdo = new PDO(
            "mysql:dbname=".self::DEFAULT_SQL_DTB.";host=".self::DEFAULT_SQL_HOST,
            self::DEFAULT_SQL_USER,
            self::DEFAULT_SQL_PASS,
            [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET CHARACTER SET UTF8',
            ]
        );
    }

    /**
     * @return DBConnextionInterface
     */
    public static function getInstance():DBConnectionInterface
    {
        if(is_null(self::$instance))
        {
          self::$instance = new DBConnection();
        }
        return self::$instance;
    }

    /**
     * 
     * @param string $query
     * @param string $fetch = 'fetchAll'
     * @return array
     */
    public function query(string $query, string $fetch = 'fetchAll'): array{
        try
        {
            $req = $this->pdo->query($query);
            $req->execute();

            $result = $fetch === 'fetch' ? $req->fetch() :  $req->fetchAll();
            $req->closeCursor();
            return $result;

        }catch(Exception $e)
        {
            new RequestDBException($e->getMessage());
        }

        
        return [];
    }

    /**
     * 
     * @param string $query
     * @param ArrayValue $values
     * @param string $fetch
     * @param bool $returnID = false
     */
    public function prepare(string $query, ArrayValue $values, string $fetch = 'fetchAll', bool $returnID = false) 
    {
        try 
        {
            $req = $this->pdo->prepare($query);

            foreach($values->getValues() as $value) 
            {
                $req->bindValue($value->getParam(), $value->getType() == 'int' ? (int)$value->getValue(): $value->getValue(), $this->getTypePDO($value->getType()));
            }
            
            $req->execute();

            switch($fetch) 
            {
                case 'fetch':
                    $result = $req->fetch();
                    break;
                case 'fetchAll':
                    $result = $req->fetchAll();
                    break;
                case 'count':
                    $result = $req->rowCount();
                    break;
                case 'update':
                    $result = $req->rowCount();
                    break;
                case 'insert':
                    if($returnID)
                    {
                        $result = $this->pdo->lastInsertId();
                    }else
                    {
                        $result = $req->rowCount();
                    }
                    break;
                case 'delete':
                    $result = $req->rowCount();
                    break;
            }

            $req->closeCursor();

            return $result;

        }catch(Exception $e) {
            new RequestDBException($e->getMessage());
        }

        return [];
    }

    /** 
     * 
    * @param string $value
    * @return int 
    */
    private function getTypePDO(string $value = "string") : int 
    {
        switch($value)
        {
            case 'string':
                $type = PDO::PARAM_STR;
                break;
            case 'int':
                $type = PDO::PARAM_INT;
                break;
            case 'float':
                $type = PDO::PARAM_STR;
                break; 
            case 'null':
                $type = PDO::PARAM_NULL;
                break;   
            default:
                $type = PDO::PARAM_STR;
            break;
        }
        
        return $type;
    }

    /**
     * 
     */
    public function __clone() 
    {
        $e = new NotCloneException();
        $e->errorClone('Cet objet ne peut pas être cloné');
    }

}
