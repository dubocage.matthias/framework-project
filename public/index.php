<?php

error_reporting(E_ALL);

require '../vendor/autoload.php';

header('Referrer-Policy: no-referrer');
header('Strict-Transport-Security: max-age=31536000; includeSubDomains; preload');
header('X-Content-Type-Options: nosniff');
header('X-Frame-Options: deny');
header('Access-Control-Allow-Origin: https://home-framework.test');
header('Content-Security-Policy: default-src \'self\'  https://stackpath.bootstrapcdn.com  https://code.jquery.com https://cdn.jsdelivr.net *.acsta.net');

use App\Exceptions\NotFoundException;
use Router\Router;

define('IMAGES', dirname($_SERVER['SCRIPT_NAME'] . DIRECTORY_SEPARATOR));
define('VIEWS', dirname(__DIR__) . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR);
define('SCRIPTS', dirname($_SERVER['SCRIPT_NAME'] . DIRECTORY_SEPARATOR ));

$router = new Router($_GET['url']);

/*------------------------*/ 
/*------------------------*/ 
/*----------WEB-----------*/ 
/*------------------------*/ 
/*------------------------*/ 

/*-----authentication-----*/
$router->get(   '/connection'       ,   'App\Controllers\AuthenticationController@connection');
$router->post(  '/connection'       ,   'App\Controllers\AuthenticationController@validatedConnection');
$router->get(   '/registration'     ,   'App\Controllers\AuthenticationController@registration');
$router->post(  '/registration'     ,   'App\Controllers\AuthenticationController@validatedRegistration');
$router->get(   '/deconnection'     ,   'App\Controllers\AuthenticationController@deconnection');

/*----------Home----------*/
$router->get(   '/home'             ,   'App\Controllers\HomeController@index');
$router->get(   '/'                 ,   'App\Controllers\HomeController@index');

/*---------Movie----------*/
$router->get(   '/movies'           ,   'App\Controllers\MovieController@movies');
$router->get(   '/random'           ,   'App\Controllers\MovieController@random');
$router->get(   '/movie/:id'        ,   'App\Controllers\MovieController@show');

/*-------User_Movie-------*/
$router->post(  '/addView'          ,   'App\Controllers\User_MovieController@addView');
$router->get(   '/view/:username'   ,   'App\Controllers\User_MovieController@viewsUser');

/*------CHARACTERE--------*/
$router->get(   '/character'        ,   'App\Controllers\MovieController@addCharacter');

/*------------------------*/ 
/*------------------------*/ 
/*----------API-----------*/ 
/*------------------------*/ 
/*------------------------*/ 

$router->get(   '/api/movies'       ,   'App\Controllers\MovieController@movies');
$router->get(   '/api/random'       ,   'App\Controllers\MovieController@random');

try{
    $router->run();
}catch (NotFoundException $e){
    echo $e->error404();
}