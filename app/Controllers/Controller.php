<?php

namespace App\Controllers;

use Database\DBConnection;

use App\Business\AuthenticationBusiness;
use Logger\Logger;

abstract class Controller
{
    public function __construct()
    {

        if(session_status() === PHP_SESSION_NONE)
        {
            session_start();
        }

        $this->changeLang();
        $this->chargeLang();
    }

    /**
     * La function du controller qui permet d'afficher les données soit en json soit en chargent la vue
     * @param string $path
     * @param array
     * @return bool
     */
    public function view(string $path, array $params = null) : bool
    {
        if(strpos($_GET['url'],'api/') !== false )
        {
            header('Content-Type: application/json');
            echo json_encode($params);
        }else
        {
            ob_start();
            $path = str_replace('.',DIRECTORY_SEPARATOR, $path);
            require VIEWS . $path . '.php';
            $content = ob_get_clean();
            require VIEWS . 'layout.php';
        }
        return true;
    }

    /**
     * La function du controller qui permet de vérifier si l'utilisateur a une session active et de le rediriger si il ne l'ai pas
     */
    public function isAuth()
    {
        $authBusiness = new AuthenticationBusiness(DBConnection::getInstance());

        if( isset($_SESSION['token']) && isset($_SESSION['username']) )
        {
            if($authBusiness->tokenIsValide($_SESSION['token'],$_SESSION['username']))
            {
                if($authBusiness->tokenIsExpired($_SESSION['token']))
                {
                    $_SESSION['token'] = $authBusiness->tokenUpdate($_SESSION['username'],$_SESSION['token']);
                    return true;
                }else
                {
                    $_SESSION['expired'] = true;
                    return header('Location: /deconnection');
                }
            }else
            {
                var_dump($_SESSION['token'],$_SESSION['username']);
                die("coucou");
            }
        }
        return header('Location: /connection');
    }

    /**
     * La function du controller qui permet de générer un tableau 
     */
    protected function returnsData(array $data):array 
    {
        foreach($_POST as $key => $value)
        {
            $data[$key] = $value ;
        }

        return $data;
    }

    /**
     * 
     */
    protected function errorForm(array $data, array $errors, string $msgLogger, string $levelLogger): array
    {
        $_SESSION['errors'] = $errors;
        Logger::addLog($msgLogger,$levelLogger,$_SERVER['REMOTE_ADDR']);
        return $this->returnsData($data);
    }

    /**
     * 
     */
    protected function changeLang() : void 
    {
        if(isset($_GET['lang']))
        {
            $_SESSION['lang'] = $_GET['lang'];  
        }else
        {
            if(!isset($_SESSION['lang']))
            {
                $_SESSION['lang'] = 'fr';
            }
        }
    }

    /**
     * @return void
     */
    protected function chargeLang() : void 
    {
        if( $_SESSION['lang'] == 'FR' || $_SESSION['lang'] == 'fr')
        {
            include('../lang/lang_fr_FR.php');
        }else
        {
            include('../lang/lang_en_EN.php');
        }
    }
}