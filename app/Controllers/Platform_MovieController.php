<?php

namespace App\Controllers;

use Database\DBConnection;
use App\Business\Platform_MovieBusiness;

use Exception;

class Platform_MovieController extends Controller
{   
    function __construct()
    {
        parent::__construct();
        //$this->isAuth();
        $this->platform_MovieBusiness = new Platform_MovieBusiness(DBConnection::getInstance());
    }

}