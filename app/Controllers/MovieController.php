<?php

namespace App\Controllers;

use Database\DBConnection;
use Library\HTML_Dome_Parser\simple_html_dom;
use App\Business\MovieBusiness;
use App\Validators\Validator;

use Exception;

class MovieController extends Controller
{   
    private $movieBusiness;

    function __construct()
    {
        parent::__construct();
        //$this->isAuth();
        $this->movieBusiness = new MovieBusiness(DBConnection::getInstance());
    }

    /**
     * 
     */
    public function movies()
    {
        $data = [];
        $page = 1;
        $search = "";

        if(isset($_GET['page'])){ 
            $page = (int)$_GET['page'];
        }
        if(isset($_GET['search'])){ 
            $search = $_GET['search'];
            $data['search'] = $_GET['search'];
        }

        if(!isset($_SESSION['username'])){
            $data = array_merge($this->movieBusiness->allMovies($page,$search),$data);
        }else{
            $data = array_merge($this->movieBusiness->allMovies($page,$search,$_SESSION['username']),$data);
        }

        return $this->view('movie.all',compact('data'));
    }

    /**
     * 
     */
    public function random()
    {
        $data = [];
        
        if(!isset($_SESSION['username'])){
            $data = array_merge($this->movieBusiness->random(),$data);
        }else{
            $data = array_merge($this->movieBusiness->random($_SESSION['username']),$data);
        }

        return $this->view('movie.show',compact('data'));
    }

    /***
     * 
     */
    public function show(int $id)
    {
        $validator = new Validator(['id' => $id]);
        $errors = $validator->validate([
            'id'  => ['required', 'idValue'],
        ]);

        if(count($errors) === 0){
            if(!isset($_SESSION['username'])){
                $data = $this->movieBusiness->show($id);
            }else{
                $data = $this->movieBusiness->show($id,$_SESSION['username']);
            }
        }

        return $this->view('movie.show',compact('data'));
    }

    /**
     * 
     */
    public function addMovies()
    {
        $month =    ['janvier' => '01' , 'février' => '02' , 'mars' => '03', 'avril' => '04', 'mai' => '05', 'juin' => '06', 
                        'juillet' => '07','août'=> '08' ,'septembre'=> '09','octobre'=> '10','novembre' => '11' ,'décembre'=>'12'];
            for($i=1; $i <= 7840; $i++){
                $url = "https://www.allocine.fr/films/alphabetique/?page=" . $i;
			  	try{
                    require_once("C:\laragon\www\home-framework\library\HTML_Dome_Parser\simple_html_dom.php");
                    $html = new simple_html_dom();
                    $html->load_file($url);
                    if(!is_null($html->root)){
                        $collection = $html->find('li[class=mdl] div[class=card entity-card entity-card-list cf]');
                        foreach($collection as $element){
                            $synopsis = "";
                            $synopsis .= $element->find('div[class=synopsis]')[0]->plaintext;
                            $title = $element->children(1)->children(0)->children(0)->plaintext;
                            $textdate = explode(' ',$element->children(1)->children(1)->children(0)->children(0)->plaintext) ;
                            if(sizeof($textdate) == 3 &&  ($textdate[1] == 'janvier' || $textdate[1] == 'février' || $textdate[1] == 'mars'|| $textdate[1] ==  'avril'|| $textdate[1] == 'mai'|| $textdate[1] ==  'juin' 
                            || $textdate[1] == 'juillet'|| $textdate[1] == 'août'|| $textdate[1] == 'septembre'|| $textdate[1] == 'octobre'|| $textdate[1] == 'novembre'|| $textdate[1] == 'décembre')){
                                $year = $textdate[2] . "-" . $month[$textdate[1]] . "-" . $textdate[0];
                                if(sizeof(explode("/",$element->children(1)->children(1)->children(0)->plaintext)) == 3){
                                    $textduration = explode("/",$element->children(1)->children(1)->children(0)->plaintext)[1];
                                    $duration = intval(explode('h', $textduration)[0])*60 + intval(explode('m', explode('h', $textduration)[1])[0]) ;
                                    if($element->children(1)->children(1)->children(1) != null){
                                        if($element->children(1)->children(1)->children(1)->children(0)->plaintext == "De" )
                                            $director = $element->children(1)->children(1)->children(1)->children(1)->plaintext;
                                        else{
                                            if($element->children(1)->children(1)->children(2) != null){
                                                $director = $element->children(1)->children(1)->children(2)->children(1)->plaintext;
                                            }
                                        }
                                        if( $element->children(0)->children(0)->children(0)->getAttribute('data-src') == null){
                                            $poster = $element->children(0)->children(0)->children(0)->getAttribute('src');
                                        }else{
                                            $poster = $element->children(0)->children(0)->children(0)->getAttribute('data-src');
                                        }
                                        $title = str_replace("&#039;" ,"'", $title);
                                        $title = str_replace("&quot;" ,"\"", $title);
                                    
                                        $data = $this->movieBusiness->addMovie($title,$year,$duration,$poster,$synopsis);
                                    }
                                }
        
                            }
                        }
                    }else{
                        var_dump("OUPS l'ouverture de la page ".$i." a échoué.<br>");
                    }
				}catch(Exception $e){
				  	var_dump($e->getMessage()."<br>");
				}
            }
            var_dump("Finish");
    }
}
