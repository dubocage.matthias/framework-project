<?php

namespace App\Controllers;

use Database\DBConnection;
use App\Business\User_MovieBusiness;
use App\Validators\Validator;
use Logger\Logger;
use Logger\LoggerLevel;

class User_MovieController extends Controller
{   
    private $user_MovieBusiness;

    function __construct()
    {
        parent::__construct();
        $this->isAuth();
        $this->user_MovieBusiness = new User_MovieBusiness(DBConnection::getInstance());
    }

    /**
     * 
     */
    public function addView()
    {
        $validator = new Validator($_POST);
        $errors = $validator->validate([
            'id'  => ['numeric', 'required'],
        ]);

        if(count($errors) === 0)
        {
            $data = [];
            $data = array_merge($this->user_MovieBusiness->addView($_SESSION['username'],$_POST['id']));
            if(!isset($data['error']))
            {
                $_SESSION['validations'][] = $data['validation'];
            }else
            {
                $_SESSION['errors'][] = $data['error'];
                Logger::addLog($data['error'],LoggerLevel::ERROR,$_SERVER['REMOTE_ADDR']);
            }
        }else{
            Logger::addLog($errors['id'],LoggerLevel::ERROR,$_SERVER['REMOTE_ADDR']);
        }

        return header('Location: /movie/'.$_POST['id']);
    }

    /**
     * 
     * @param string $username
     */
    public function viewsUser(string $username)
    {
        !isset($_GET['page']) ? $page = 1 : $page = (int)$_GET['page'];
       
        $search = "";
        if(isset($_GET['search']))
        { 
            $search = $_GET['search'];
            $data['search'] = $_GET['search'];
        }

        $validator = new Validator(['username'=>$username,'page'=>$page,'search'=>$search]);
        $errors = $validator->validate([
            'username'  => ['username', 'required'],
            'page'  => ['numeric'],
            'search'  => ['numChar'],
        ]);

        if(count($errors) === 0)
        {
            $data = [];
            $data = array_merge( $this->user_MovieBusiness->viewUser($_SESSION['username'],$page,$search),$data);

            return $this->view('profile.view',compact('data'));
        }else
        {
            foreach($errors as $error){
                Logger::addLog($error,LoggerLevel::ERROR,$_SERVER['REMOTE_ADDR']);
            }
        }
        return header('Location: /home');
    }
}