<?php

namespace App\Controllers;

use Database\DBConnection;
use App\Business\AuthenticationBusiness;
use Logger\LoggerLevel;
use App\Validators\Validator;
use Logger\Logger;

class AuthenticationController extends Controller
{
    /**
     * La function du controller qui permet de redirectioner sur la page de connection
     */
    public function connection()
    {
        $authBusiness = new AuthenticationBusiness(DBConnection::getInstance());

        if( isset($_SESSION['token']) && isset($_SESSION['username']) && 
            $authBusiness->tokenIsValide($_SESSION['token'],$_SESSION['username']))
        {
            return header('Location: /home');
        }

        return $this->view('auth.connection');
    }

    /**
     * La function du controller qui permet de redirectioner sur la page d'inscription 
     */
    public function registration()
    {
        $authBusiness = new AuthenticationBusiness(DBConnection::getInstance());

        if( isset($_SESSION['token']) && isset($_SESSION['username']) && 
            $authBusiness->tokenIsValide($_SESSION['token'],$_SESSION['username']))
        {
            return header('Location: /home');
        }

        return $this->view('auth.registration');
    }

    /**
     * La function du controller qui permet de déconnecter un utilisateur et de le redirectionner 
     */
    public function deconnection()
    {
        $expired = $_SESSION["expired"];
        Logger::addLog("L'utilisateur ".$_SESSION['username']." c'est déconnecté.",LoggerLevel::INFO,$_SERVER['REMOTE_ADDR']);
        session_unset();
        session_destroy();

        if($expired){
            session_start();
            $_SESSION['errors'] = ['Votre session a expiré.'];
        }
           
        return header('Location: /');
    }

    /**
     * La function du controller qui permet de valider les données de connection 
     */
    public function validatedConnection()
    {
        $validator = new Validator($_POST);
        $errors = $validator->validate([
            'username'  => [ 'username', 'required'],
            'password'  => [ 'password', 'required'],
        ]);

        if(count($errors) === 0)
        {
            $authBusiness = new AuthenticationBusiness(DBConnection::getInstance());
            $data = $authBusiness->authentification($_POST['username'],$_POST['password']);

            if(!isset($data['error']))
            {
                $_SESSION['token'] = $data['token'];
                $_SESSION['username'] = $data['username'];
                $_SESSION['mail'] = $data['mail'];
                $_SESSION['validations'] = ["Vous êtes bien connecté ".$data['username']."."];
                Logger::addLog("L'utilisateur ".$_POST['username'] ." viens de ce connecter.",LoggerLevel::INFO,$_SERVER['REMOTE_ADDR'] );

                return header('Location: /home');

            }else{
                $data = $this->errorForm($data,$data['error'],"Quequ'un a chercher a ce connecter avec cette username : ".$_POST['username'] ." || ".implode(',',$data['error']),LoggerLevel::WARNING);
            }
        }else{
           $data = $this->errorForm([],["Le mot de passe ou le pseudo est incorrect"],"La connection a échoué : ".implode(',',$errors),LoggerLevel::ERROR);
        }

        return $this->view('auth.connection',compact('data'));
    }

    /**
     * La function du controller qui permet de valider les données d'inscription
     */
    public function validatedRegistration()
    {
        $validator = new Validator($_POST);
        $errors = $validator->validate([
            'username'  => [ 'username', 'required'],
            'password'  => [ 'password', 'required'],
            'mail'      => [ 'mail', 'required']
        ]);
        
        if(count($errors) === 0){

            $authBusiness = new AuthenticationBusiness(DBConnection::getInstance());
            $data = $authBusiness->inscription($_POST['username'],$_POST['password'],$_POST['mail']);

            if (!isset($data['error'])){

                $_SESSION['validations'] = $data['validation'];
                Logger::addLog("Un nouveau utilisateur nous à rejoint : ".$_POST['username'],LoggerLevel::INFO,$_SERVER['REMOTE_ADDR'] );
                return header('Location: /connection');

            }else{
                $data = $this->errorForm($data,$data['error'],"Une inscription a échoué : ".implode(',',$data['error']),LoggerLevel::ERROR);
            }
            
        }else{
            $data = $this->errorForm([],$errors,"Une inscription a échoué : ".implode(',',$errors),LoggerLevel::ERROR);
        }

        return $this->view('auth.registration',compact('data'));
    }
}