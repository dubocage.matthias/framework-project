<?php

namespace App\Business;

use App\Business\DAOs\DAOPlatform_Movie;
use App\Business\DAOs\DAOPlatform;
use App\business\DAOs\DAOMovie;

class Platform_MovieBusiness extends ModelBusiness
{
    /**
     * @param string $titleMovie
     * @param string $releaseDateMovie
     * @param string $platform
     * @param string $link
     * @return array
     */
    public function addPlatform_Movie(string $titleMovie, string $releaseDateMovie, string $platform, string $link): array
    {
        $data = [];

        $daoPlatform = new DAOPlatform($this->db);
        $daoMovie = new DAOMovie($this->db);
        $daoPlatform_movie = new DAOPlatform_movie($this->db);

        $idPlatform = $daoPlatform->getPlatform($platform)->getId();
        $idMovie = $daoMovie->getId($titleMovie,$releaseDateMovie);

        if(!$daoPlatform_movie->linkExist($idPlatform,$idMovie)){
            if($daoPlatform_movie->insert($idPlatform,$idMovie,$link)){
                $data['validation'] = 'Nous avons rajouté un lien au film'.$titleMovie.' sur la platforme'.$platform.'.';
            }else{
                $data['error'] = 'Nous n\'avons pas rajouté un lien au film '.$titleMovie.' sur la platforme'.$platform.'.';
            }
        }else{
            $data['error'] = 'Nous avons déja rajouté un lien au film '.$titleMovie.' sur la platforme'.$platform.'.';
        }

        return $data;
    }

    /**
     * 
     * @param string $platform
     * @return array
     */
    public function deleteLinkPlatform(string $platform): array
    {
        $data = [];
        
        $daoPlatform_movie = new DAOPlatform_movie($this->db);

        if($daoPlatform_movie->deleteLinkPlatform($platform)){
            $data['validation'] = 'La suppression des liens de la platforme '.$platform.' a fonctionnée!';
        }else{  
            $data['error'] = 'La suppression des liens de la platforme '.$platform.' n\'a pas fonctionnée!';
        }

        return $data;
    }

}