<?php

namespace App\Business;

use App\Business\DAOs\DAOPlatform;

class PlatformBusiness extends ModelBusiness
{
    /**
     * 
     * @return array
     */
    public function allPlatform(): array
    {
        $data = [];
        
        $daoPlatform = new DAOPlatform($this->db);
        $data['platforms'] = $daoPlatform->allPlatform();

        return $data;
    }

    /**
     * 
     * @param string $name
     * @return array
     */
    public function getPlatform(String $name): array
    {
        $data = [];

        $daoPlatform = new DAOPlatform($this->db);
        $data['platform'] = $daoPlatform->getPlatform($name);

        return $data;
    }
  
}