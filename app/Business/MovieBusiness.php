<?php

namespace App\Business;

use App\Business\DAOs\DAOMovie;
use App\Business\Models\Movie;
use App\Business\DAOs\Interfaces\DBConnectionInterface;

use DateTime;

class MovieBusiness extends ModelBusiness
{

    /*Le nombre maximum de film sur une page*/
    private const NB_MOVIE_ON_PAGE = 10;
    private $daoMovie;

    public function __construct(DBConnectionInterface $db)
    {
        parent::__construct($db);
        $this->daoMovie = new DAOMovie($this->db);
    }

    /**
     * La function business pour récupérer touts les films d'une page donnée avec le nombre de vus par film de l'utilisateur si connecter ainsie que le nombre de page par recherche 
     * @param int $page
     * @param string $search
     * @param string $username
     * @return array
     */
    public function allMovies(int $page = 1, String $search = '',String $username = ''): array
    {
        $data = [];
        //Récupère le nombre de page de la recherche 
        $data['nbPage'] = $this->getNbPage($search);

        //Si la page est incorrect on la repasse à la première
        $page > $data['nbPage'] ?? $page = 1;
           
        $data['page'] = $page;

        //Récupère la liste des films 
        $data['movies'] = $this->daoMovie->allMovies(($page-1) * self::NB_MOVIE_ON_PAGE,self::NB_MOVIE_ON_PAGE,$search,$username);

        return $data;
    }

    /**
     * La function business qui récupère le nombre de page lié à la recherche
     * @param string $search
     * @return array
     */
    public function getNbPage(string $search = '') : int
    {
        //Calcule le nombre de page en fonction du nombre de film par page;
        $nbPage = $this->daoMovie->nbMovie($search);
        if(fmod( $nbPage ,self::NB_MOVIE_ON_PAGE) > 0 ){
            return (int)( $nbPage / self::NB_MOVIE_ON_PAGE) + 1;
        }else{
             return (int)( $nbPage / self::NB_MOVIE_ON_PAGE);
        }
    }

    /**
     * La function business qui récupère un film aléatoirement dans la liste de tous les films
     * @param string $username
     * @return array
     */
    public function random(string $username=''):array
    {
        $rand = rand(1,(int)($this->daoMovie->nbMovie()));
        return $this->daoMovie->randomMovie($rand,$username);;
    }

    /**
     * 
     * @param int $idMovie
     * @param string $username
     * @return array
     */
    public function show(int $idMovie , string $username = ''):array
    {
        return $this->daoMovie->show($idMovie ,$username);
    }
 
    /**
     * 
     * @param string $title
     * @param string $releaseDate
     * @param int $duration
     * @param string $poster
     * @param string $synopsis
     * @return array
     */
    public function addMovie(string $title, string $releaseDate, int $duration, string $poster, string $synopsis): array
    {    
        $data = [];

        $movie = new Movie();
        $movie->setTitle($title);
        $movie->setReleaseDate(new DateTime($releaseDate));
        $movie->setDuration($duration);
        $movie->setPoster($poster);
        $movie->setSynopsis($synopsis);

        if(!$this->daoMovie->exist($movie)){
            if($this->daoMovie->insertMovie($movie)){
                $data['validation'] = "Le filme à été correctement ajouté";
            }else{
                $data['error'] = "Le filme n'a pas été correctement ajouté.";
            }
        }else{
            $data['error'] = 'Le film à déja été rajouté.';
        }

        return $data;
    }

    /**
     * 
     * @param string $title
     * @param string $releaseDate
     * @return bool
     */
    public function movieExiste(string $title,string $releaseDate): bool
    {
        $movie = new Movie();
        $movie->setTitle($title);
        $movie->setReleaseDate(new DateTime($releaseDate));

        return $this->daoMovie->exist($movie);
    }
}