<?php

namespace App\Business\Models;

use JsonSerializable;

abstract class Model implements JsonSerializable 
{
    protected $id;

    public function __construct(int $id = null)
    {
      if(isset($id))
      {
          $this->id = $id;
      }
    }

    /**
     * 
     */
    public function hydrate(array $datas) : void
    {
      foreach ($datas as $key => $value)
      {
        if(!empty($key))
        {
          $method = 'set'.ucfirst($key);
              
          if(method_exists($this, $method))
          {
            $this->$method($value);
          }
        }
      }
    }

    public function getId() : int 
    {
      return $this->id;
    }

    public function setId(int $id) : void
    {
      $this->id = $id;
    }
}