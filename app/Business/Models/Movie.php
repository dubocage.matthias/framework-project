<?php 

namespace App\Business\Models;

use DateTime;

class Movie extends Model{

    private $title;
    private $releaseDate;
    private $duration;
    private $score;
    private $synopsis;
    private $poster;

    public function __construct(){
        parent::__construct();
        $this->title = "";
        $this->score = 0.0;
        $this->synopsis = "";
        $this->duration = 0;
        $this->poster = "";
        $this->releaseDate = new DateTime();
    }

    public function getTitle():string{
        return $this->title;
    }
    
    public function setTitle(string $title):void{
        $this->title = $title;
    }

    public function getReleaseDate():DateTime{
        return $this->releaseDate;
    }

    public function setReleaseDate($releaseDate):void{
        if(gettype($releaseDate) == 'string'){
            $this->releaseDate = DateTime::createFromFormat('d/m/Y', $releaseDate);
        }else{
            if( $releaseDate instanceof DateTime){
                $this->releaseDate = $releaseDate;
            }
        }
    }

    public function getDuration():int{
        return $this->duration;
    }

    public function setDuration(int $duration):void{
        $this->duration = $duration;
    }

    public function getScore():float{
        return $this->score;
    }

    public function setScore(float $score):void{
        $this->score = $score;
    }

    public function getSynopsis():string{
        return $this->synopsis;
    }

    public function setSynopsis(string $synopsis):void{
        $this->synopsis = $synopsis;
    }

    public function getPoster():string{
        return $this->poster;
    }

    public function setPoster(string $poster):void{
        $this->poster = $poster;
    }

    public function jsonSerialize() : array
    {
      foreach(get_class_vars(get_class($this)) as $name => $value){
            $method = "get".ucfirst($name);
            $array[$name] = $this->$method();
      }

      return $array;
    }
}