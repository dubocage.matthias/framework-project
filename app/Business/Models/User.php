<?php

namespace App\Business\Models;

use DateTime;

class User extends Model
{
    private $username;
    private $password;
    private $token;
    private $update_token;
    private $salt;
    private $mail;

    function __construct() 
    {
        parent::__construct();
        $this->username = "";
        $this->password = "";
        $this->token = "";
        $this->update_token = new DateTime();
        $this->salt = "";
        $this->mail = "";
    }

    function getUsername(): string 
    {
        return $this->username;
    }

    function getPassword(): string 
    {
        return $this->password;
    }

    function getToken(): string 
    {
        return $this->token;
    }

    function getUpdateToken(): DateTime 
    {
        return $this->update_token;
    }

    function getSalt(): string 
    {
        return $this->salt;
    }

    function getMail(): string 
    {
        return $this->mail;
    }

    function setUsername(string $username) 
    {
        $this->username = $username;
    }

    function setPassword(string $password) 
    {
        $this->password = $password;
    }

    function setToken(string $token) 
    {
        $this->token = $token;
    }

    function setUpdate_Token(string $update_token) 
    {
        $this->update_token = new DateTime($update_token);
    }

    function setSalt(string $salt) 
    {
        $this->salt = $salt;
    }

    function setMail(string $mail)
    {
        $this->mail = $mail;
    }

    public function jsonSerialize() : array
    {
      foreach(get_class_vars(get_class($this)) as $name => $value){
            $method = "get".ucfirst($name);
            $array[$name] = $this->$method();
      }

      return $array;
    }

}