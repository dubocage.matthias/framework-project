<?php

namespace App\Business\Models;

abstract class Genre{
    
    const ACTION            = 'ACTION';
    const ANIMATION         = 'ANIMATION';
    const ADVENTURE         = 'ADVENTURE';
    const BIOPIC            = 'BIOPIC';
    const BOLLYWOOD         = 'BOLLYWOOD';
    const CLASSIC           = 'CLASSIC';
    const COMEDY            = 'COMEDY';
    const CRIME_STORY       = 'CRIME_STORY';
    const CONCERT           = 'CONCERT';
    const CARTOON           = 'CARTOON';
    const DRAMA             = 'DRAMA';  
    const DOCUMENTARY       = 'DOCUMENTARY';
    const HORROR            = 'HORROR';
    const EROTIC            = 'EROTIC';
    const ESPIONAGE         = 'ESPIONAGE';
    const EXPERIMENTAL      = 'EXPERIMENTAL';
    const FAMILY            = 'FAMILY';
    const FANTASTIC         = 'FANTASTIC';
    const HISTORICAL        = 'HISTORICAL';
    const JUDICIAL          = 'JUDICIAL';
    const MARTIAL_ARTS      = 'MARTIAL_ARTS';
    const MEDICAL           = 'MEDICAL';
    const MUSICAL_COMEDY    = 'MUSICAL_COMEDY';
    const PEPLUM            = 'PEPLUM';
    const ROMANCE           = 'ROMANCE';
    const SCIENCE_FICTION   = 'SCIENCE_FICTION';
    const THRILLER          = 'THRILLER';
    const VARIOUS           = 'VARIOUS';
    const WAR               = 'WAR';
    const WESTERN           = 'WERSTER';
}