<?php 

namespace App\Business\Models;

class Platform extends Model 
{

    private $name;
    private $link;

    public function __construct() 
    {
        parent::__construct();
        $this->name = "";
        $this->link = "";
    }

    public function getName() : String 
    {
        return $this->name;
    }

    public function getLink() : String 
    {
        return $this->link;
    }

    public function setName(String $name) 
    {
        $this->name = $name;
    }

    public function setLink(String $link) 
    {
        $this->link = $link;
    }

    public function jsonSerialize() : array 
    {
      foreach(get_class_vars(get_class($this)) as $name => $value) 
      {
            $method = "get".ucfirst($name);
            $array[$name] = $this->$method();
      }
      return $array;
    }
}