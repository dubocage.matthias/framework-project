<?php

namespace App\Business;

use App\Business\DAOs\DAOUser;
use App\Business\DAOs\DAOUser_Movie;
use App\Business\DAOs\Interfaces\DBConnectionInterface;

class User_MovieBusiness extends ModelBusiness
{

    /*Le nombre maximum de film sur une page*/
    private const NB_MOVIE_VIEW_ON_PAGE = 10;
    private $daoUser_Movie;

    public function __construct(DBConnectionInterface $db)
    {
        parent::__construct($db);
        $this->daoUser_Movie = new DAOUser_Movie($this->db);
    }


    /**
     * 
     * @param string $username
     * @param int $idMovie
     * @return array
     */
    public function addView(string $username,int $idMovie) : array{

        $daoUser = new DAOUser($this->db);
        $idUser = $daoUser->getIdByUsername($username);

        $data = [];

        if($this->daoUser_Movie->viewExist($idUser,$idMovie)){
            $nb = $this->daoUser_Movie->getNbView($idUser,$idMovie) + 1;
            if($this->daoUser_Movie->update($idUser,$idMovie,$nb)){
                $data['validation'] = "Bravos vous avez gagné 15xp!";
            }else{
                $data['error'] = "Une erreur c'est produite.";
            }
        }else{
            if($this->daoUser_Movie->insert($idUser,$idMovie)){
                $data['validation'] = "Bravos vous avez gagné 15xp!";
            }else{
                $data['error'] = "Une erreur c'est produite.";
            }
        }

        return $data;
    }

    /**
     * 
     * @param string $username
     * @return array
     */
    public function viewUser(string $username, int $page = 1, string $search = '') : array
    {
        $data = [];
        
        $data['username'] = $username;
        $data['nbPage'] = $this->getNbPage($username,$search);
        $page > $data['nbPage'] ?? $page = 1;
        $data['page'] = $page;

        $data['movies'] = $this->daoUser_Movie->allMovieViewByUser($username,($page-1) * self::NB_MOVIE_VIEW_ON_PAGE,self::NB_MOVIE_VIEW_ON_PAGE,$search);
        
        return $data;
    }

    /**
     * 
     * @param string $search
     * @return array
     */
    public function getNbPage(string $username, string $search = ''): int
    {
        $nbPage = $this->daoUser_Movie->nbMovieViewByUser($username,$search);
        if(fmod( $nbPage ,self::NB_MOVIE_VIEW_ON_PAGE) > 0 ){
            return (int)( $nbPage / self::NB_MOVIE_VIEW_ON_PAGE) + 1;
        }else{
             return (int)( $nbPage / self::NB_MOVIE_VIEW_ON_PAGE);
        }

    }

}