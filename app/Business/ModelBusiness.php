<?php

namespace App\Business;

use App\Business\DAOs\Interfaces\DBConnectionInterface;

abstract class ModelBusiness
{
    protected $db;

    function __construct(DBConnectionInterface $db)
    {
        $this->db = $db;
    }
}