<?php

namespace App\Business\Utils;
use DateTime;


class Utils
{
    //Les caractères utilisable pour la génération du salt 
    private const CHARACTERS = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    //Le nombre de caractères de la constante CHARACTERS
    private const HEIGHT = 42;
    private const IV = 'adB$i62PR;39#nS^' ;
    //Le poivre
    private const PEPPER = 'bYc*MKED!26DFls';
    private const SECRET = '97ac471f67a858f6b8c512947035d3087e142c719a8207a5a956d44517cf9e13';

    /**
     * La function génère un token aléatoire
     * @return string
     */
    public static function generateToken(int $minutes,string $typToken): string
    {
        $header = json_encode([
            'typ' => $typToken,
            'alg' => 'HS256'
        ]);

        $timestamp = new DateTime();
        $timestamp->modify("+".$minutes." minutes");
        $timestamp = $timestamp->getTimestamp();
        $payload = json_encode([
            'exp' => $timestamp
        ]);
        
        $base64UrlHeader = self::base64UrlEncode($header);
        $base64UrlPayload = self::base64UrlEncode($payload);
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, self::SECRET, true);
        $base64UrlSignature = self::base64UrlEncode($signature);
        $jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;

        return $jwt;
    }

    /**
     * La function génère un sel aléatoire
     * @return string
     */
    public static function generateSalt(): string
    {
        $heightMax = strlen(self::CHARACTERS);
        $stringRandom = '';

        for ($i = 0; $i < self::HEIGHT; $i++)
        {
            $stringRandom .= self::CHARACTERS[rand(0, $heightMax - 1)];
        }

        $stringRandom = str_shuffle($stringRandom);

        return $stringRandom;
    }

    /**
     * Function qui permet de hasher une donnée
     * @param string $data
     * @param string $salt
     * @return string
     */
    public static function hashData(string $data, string $salt): string
    {
        $data = self::cryptData($data, $salt);
        return  hash('sha3-256',$data);
    }

    /**
     * Function qui permet de chiffrer la donnée
     * @param string $data
     * @return string
     */
    public static function cryptData(string $data, string $salt): string
    {
        $data  = openssl_encrypt($data, 'AES-128-CBC', $salt, 0, self::IV);
        $data  = openssl_encrypt($data, 'AES-128-CBC', self::PEPPER ,0, self::IV); 

        return $data;
    }

    /**
     * Function qui permet de déchiffrer la donnée
     * @param string $data
     * @return string
     */
    public static function decryptData(string $data, string $salt): string
    {
        $data  = openssl_decrypt($data, 'AES-128-CBC', self::PEPPER, 0, self::IV); 
        $data  = openssl_decrypt($data, 'AES-128-CBC', $salt, 0, self::IV);

        return $data;
    }

    /**
     * 
     */
    public static function base64UrlEncode($text): string
    {
        return str_replace(
            ['+', '/', '='],
            ['-', '_', ''],
            base64_encode($text)
        );
    }

    /**
     * 
     * @param string $token
     * @return bool 
     */
    public static function ValidateToken(string $token): bool
    {

        $tokenParts = explode('.', $token);
        $header = base64_decode($tokenParts[0]);
        $payload = base64_decode($tokenParts[1]);
        $signatureProvided = $tokenParts[2];

        $expiration =  (new DateTime)->setTimestamp(json_decode($payload)->exp);
        $now = new DateTime();

        if( $now >= $expiration )
        {
            return false;
        }

        $base64UrlHeader = self::base64UrlEncode($header);
        $base64UrlPayload = self::base64UrlEncode($payload);
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, self::SECRET, true);
        $base64UrlSignature = self::base64UrlEncode($signature);

        if ($base64UrlSignature !== $signatureProvided)
        {
            return false;
        }

        return true;
    }

}