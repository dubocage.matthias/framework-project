<?php

namespace App\Business;

use App\Business\DAOs\DAOUser;
use App\Business\Models\User;
use App\Business\Utils\Utils;

class AuthenticationBusiness extends ModelBusiness
{
    private static $EXPIRY = 60;

    /**
     * La fonction permet de vérifier l'username et le password soit valide
     * @param string $username
     * @param string $password
     * @return bool
     */
    private function isValide(string $username, string $password): bool
    {
        $daoUser = new DAOUser($this->db);

        $user = $daoUser->getUserByUsername($username);
        
        if($user->getSalt() != '' && $user->getPassword() != ''){
            if( Utils::hashData($password, $user->getSalt()) ===  $user->getPassword()){
                return true;
            }
        }

        return false;
    }

    /**
     * La function vérifie que le token correspond bien à l'utilisateur
     * @param string $token
     * @param string $username
     * @return bool
     */
    public function tokenIsValide(string $token, string $username): bool
    {
        $daoUser = new DAOUser($this->db);
        return $daoUser->verificationToken($token,$username);
    }

    /**
     * La function vérifie que le token n'est pas expired.
     * @param string $token
     * @param string $username
     * @return bool
     */
    public function tokenIsExpired(string $token): bool
    {
        return Utils::ValidateToken($token);
    }

    public function tokenUpdate(string $username, string $token): string {

        $daoUser = new DAOUser($this->db);
        $token = Utils::generateToken(self::$EXPIRY,"token");
        $daoUser->updateToken($username,$token);

        return $token;
    }

    /**
     * Function qui va permetre d'authentifier l'utilisateur
     * La fonction return un tableau avec une column error
     * @param string $username
     * @param string $password
     * @return array
     */
    public function authentification(string $username, string $password): array
    {
        $data = [];

        $daoUser = new DAOUser($this->db);

        if($this->isValide($username,$password))
        {
            $token = Utils::generateToken(self::$EXPIRY,"token");

            if($daoUser->setTokenUserByUsername($token,$username))
            {
                $user = new User();
                $user->setUsername($username);
                $daoUser->getMailByUsername($user);
                $daoUser->getSaltByUsername($user);
                $mail = Utils::decryptData($user->getMail(),$user->getSalt());

                $data['token'] = $token;
                $data['username'] = $username;
                $data['mail'] = $mail;

            } else {
                $data['error'][] = 'Une error c\'est produite !';
            }

        } else {
            $data['error'][] = 'Le mot de passe ou le pseudo est invalide !';
        }
        return $data;
    }

    /**
     * Fonction qui permet d'enregistrer les informations d'un user 
     * La fonction return un tableau avec une column error ou validation
     * @param string $username
     * @param string $password
     * @param string $mail
     * @return array 
     */
    public function inscription(string $username, string $password, string $mail) : array
    {
        $data = [];

        $daoUser = new DAOUser($this->db);

        if(!$daoUser->usernameExist($username))
        {
            $user = new User();
            $user->setUsername($username);
            $user->setSalt(Utils::generateSalt());
            $user->setMail(Utils::cryptData($mail,$user->getSalt()));
            $user->setPassword(Utils::hashData($password, $user->getSalt()));

            if($daoUser->insertUser($user)) {
                $data['validation'][] = "Vous êtes bien inscrit !";
            } else {
                $data['error'][] = "Une erreur c'est produite !";
            }
        } else {
            $data['error'][] = "Cette Username existe déja !";
        }

        return $data;
    }

}