<?php

namespace App\Business\DAOs;


class ArrayValue 
{
    private $values;

    function __construct(){
        $this->values = [];
    }

    public function addValue(Value $value){
        $this->values[] = $value;
    }

    public function getValues() : array{
        return $this->values;
    }
}