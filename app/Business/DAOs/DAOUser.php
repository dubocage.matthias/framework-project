<?php

namespace App\Business\DAOs;

use App\Business\Models\User;
use App\Business\DAOs\Value;
use App\Business\DAOs\ArrayValue;

class DAOUser extends DAOModel
{
    /**
     * 
     * @param string $username
     * @return bool
     */
    public function usernameExist(string $username): bool 
    {
        $sql=" 
            SELECT username
            FROM user 
            WHERE username = :username 
        ";
        $values = new ArrayValue();
        $values->addValue(new Value(':username', $username, 'string'));

        return $this->db->prepare($sql,$values,'count') === 1; 
    }

    /**
     * 
     * @param User $user
     * @return bool
     */
    public function insertUser(User $user): bool
    {
        $sql="
            INSERT INTO user ( username, password, salt, mail)
            VALUES ( :username, :password, :salt, :mail)
        ";
        $values = new ArrayValue();
        $values->addValue(new Value(':username', $user->getUsername(), 'string'));
        $values->addValue(new Value(':password', $user->getPassword(), 'string'));
        $values->addValue(new Value(':salt', $user->getSalt(), 'string'));
        $values->addValue(new Value(':mail', $user->getMail(), 'string'));

        return $this->db->prepare($sql,$values,'insert',true) === 1;
    }

    /**
     * 
     * @param string $username
     * @return User
     */
    public function getUserByUsername(string $username): User
    {
        $sql="
            SELECT username, password , salt 
            FROM user 
            WHERE username = :username
        ";
        $values = new ArrayValue();
        $values->addValue(new Value(':username', $username, 'string'));
        
        $user = new User();
        $result = $this->db->prepare($sql,$values,'fetch');
        
        if(is_array($result)){
            $user->hydrate($result);
        }

        return $user;
    }

    /**
     * 
     * @param string $token
     * @param string $username
     * @return bool
     */
    public function setTokenUserByUsername(string $token, string $username): bool
    {
        date_default_timezone_set('Europe/Paris');

        $sql="
            UPDATE user 
            SET token = :token, update_token = :update_token
            WHERE username = :username
        ";
        $values = new ArrayValue(); 
        $values->addValue(new Value(':username', $username, 'string'));
        $values->addValue(new Value(':token', $token, 'string'));
        $values->addValue(new Value(':update_token', date("Y-m-d H:i:s"), 'string'));

        return $this->db->prepare($sql,$values,'update');
    }

    /**
     * 
     * @param string $token
     * @param string $username
     * @return bool
     */
    public function verificationToken( string $token, string $username): bool
    {
        date_default_timezone_set('Europe/Paris');

        $sql=" 
            SELECT update_token 
            FROM user
            WHERE username = :username 
                AND token = :token
        ";
        
        $values = new ArrayValue(); 
        $values->addValue(new Value(':username', $username, 'string'));
        $values->addValue(new Value(':token', $token, 'string'));

        if($this->db->prepare($sql,$values,'count') === 1)
        {
            return true;
        }

        return false;
    }

    /**
     * 
     * @param string $username
     * @param string $token
     * @return bool
     */
    public function updateToken( string $username, string $token): bool
    {
        date_default_timezone_set('Europe/Paris');

        $sql="
            UPDATE user
            SET token = :token , update_token = :update_token
            WHERE username = :username
        ";

        $values = new ArrayValue(); 
        $values->addValue(new Value(':username', $username, 'string'));
        $values->addValue(new Value(':token', $token, 'string'));
        $values->addValue(new Value(':update_token', date("Y-m-d H:i:s"), 'string'));

        return $this->db->prepare($sql,$values,'update');
    }

    /**
     * 
     * @param User $user
     */
    public function getMailByUsername(User $user): void
    {
        $sql="
            SELECT mail
            FROM user
            where username = :username
        ";
        $values = new ArrayValue(); 
        $values->addValue(new Value(':username',$user->getUsername(), 'string'));

        $user->hydrate($this->db->prepare($sql,$values,'fetch'));
    }

    /**
     * 
     * @param User $user
     */
    public function getSaltByUsername(User $user): void
    {
        $sql="
            SELECT salt
            FROM user
            where username = :username
        ";
        $values = new ArrayValue(); 
        $values->addValue(new Value(':username', $user->getUsername(), 'string'));

        $user->hydrate($this->db->prepare($sql,$values,'fetch'));
    }

    /**
     * 
     * @param srting $username
     * @return int
     */
    public function getIdByUsername(string $username): int
    {
        $sql="
            SELECT id
            FROM user
            WHERE username = :username;
        ";
        $values = new ArrayValue(); 
        $values->addValue(new Value(':username', $username, 'string'));

        return $this->db->prepare($sql,$values,'fetch')['id'];
    }
}