<?php

namespace App\Business\DAOs;

use App\Business\DAOs\Value;
use App\Business\DAOs\ArrayValue;
use App\Business\Models\Movie;
use DateTime;

class DAOUser_Movie extends DAOModel
{
    /**
     * 
     * @param int $idUser
     * @param int $idMovie
     * @return bool
     */
    public function viewExist(int $idUser, int $idMovie) : bool
    {
        $sql=" 
            SELECT id_user
            FROM user_movie
            WHERE id_user = :id_user AND id_movie = :id_movie
        ";
        $values = new ArrayValue();
        $values->addValue(new Value(':id_user', "".$idUser, 'int'));
        $values->addValue(new Value(':id_movie', "".$idMovie, 'int'));

        return $this->db->prepare($sql,$values,'count') === 1; 
    }

    /**
     * 
     * @param int $idUser
     * @param int $idMovie
     * @return bool
     */
    public function insert(int $idUser, int $idMovie) : bool
    {
        $dateNow = new DateTime('NOW');
        $sql = "
            INSERT INTO user_movie (id_user,id_movie,created_date,updated_date,nb_view)
            VALUES (:id_user,:id_movie,:created_date,:updated_date,1);
        ";

        $values = new ArrayValue();
        $values->addValue(new Value(':id_user', "".$idUser, 'int'));
        $values->addValue(new Value(':id_movie', "".$idMovie, 'int'));
        $values->addValue(new Value(':created_date', $dateNow->format("Y-m-d H:i:s"), 'string'));
        $values->addValue(new Value(':updated_date', $dateNow->format("Y-m-d H:i:s"), 'string'));

        return $this->db->prepare($sql,$values,'insert') === 1;
    }

    /**
     * 
     * @param int $idUser
     * @param int $idMovie
     * @param int $nb_View
     * @return bool 
     */
    public function update(int $idUser, int $idMovie,int $nb_View) : bool 
    {
        $dateNow = new DateTime('NOW');
        $sql = "
            UPDATE user_movie
            SET nb_view = :nb_view, updated_date = :updated_date
            WHERE id_user = :id_user AND id_movie = :id_movie; 
        ";

        $values = new ArrayValue();
        $values->addValue(new Value(':nb_view', "".$nb_View, 'int'));
        $values->addValue(new Value(':id_user', "".$idUser, 'int'));
        $values->addValue(new Value(':id_movie', "".$idMovie, 'int'));
        $values->addValue(new Value(':updated_date', $dateNow->format("Y-m-d H:i:s"), 'string'));

        return $this->db->prepare($sql,$values,'update') === 1;
    }

    /**
     * 
     * @param int $idUser
     * @param int $idMovie
     * @return int
     */
    public function getNbView(int $idUser, int $idMovie) : int
    {
        $sql = " 
            SELECT nb_view 
            FROM user_movie
            WHERE id_user = :id_user AND id_movie = :id_movie;
        ";

        $values = new ArrayValue();
        $values->addValue(new Value(':id_user', "".$idUser, 'int'));
        $values->addValue(new Value(':id_movie', "".$idMovie, 'int'));

        $nbView = $this->db->prepare($sql,$values,'fetch');
        if($nbView == false)
        {
            $nbView = 0 ;
        }else{
            $nbView = (int) $nbView['nb_view'];
        }

        return (int) $nbView;
    }

    /**
     * @param string $username
     * @param string $search
     * @return int
     */
    public function nbMovieViewByUser(string $username, string $search = '') : int
    {
        $sql = "
            SELECT COUNT(*) 
            FROM movie 
            INNER JOIN user_movie ON user_movie.id_movie = movie.id
            INNER JOIN user ON user.id = user_movie.id_user AND user.username = :username
            WHERE title LIKE :search;
        ";

        $values = new ArrayValue();
        $values->addValue(new Value(':username',$username,'string'));
        $values->addValue(new Value(':search','%'.$search.'%','string'));

        return (int) $this->db->prepare($sql,$values,'fetch')['COUNT(*)'];
    }

    /**
     * @param string $username
     * @param string $search
     * @return int
     */
    public function allMovieViewByUser(string $username,int $pagination, int $nbMovie, string $search) : array
    {
        $sql = "
            SELECT movie.*, user_movie.nb_view, user_movie.updated_date, platform.name, platform_movie.link
            FROM movie 
            INNER JOIN user_movie ON user_movie.id_movie = movie.id 
            INNER JOIN user ON user.id = user_movie.id_user AND user.username = :username
            LEFT JOIN platform_movie ON platform_movie.id_movie = movie.id
            LEFT JOIN platform ON platform_movie.id_platform = platform.id
            WHERE  title LIKE :search
            ORDER BY DATE_FORMAT(user_movie.updated_date,'%Y-%m-%d-%H-%i-%s') DESC
            LIMIT :nbMovie OFFSET :pagination;
        ";

        $values = new ArrayValue();
        $values->addValue(new Value(':username',$username,'string'));
        $values->addValue(new Value(':search','%'.$search.'%','string'));
        $values->addValue(new Value(':pagination',"".$pagination, 'int'));
        $values->addValue(new Value(':nbMovie', "".$nbMovie, 'int'));

        $datas = [];
        
        foreach($this->db->prepare($sql,$values) as $data){
            $movie = new Movie();
            $movie->hydrate($data);
            $datas['movie_'.$movie->getId()]['movie'] = $movie;
            $datas['movie_'.$movie->getId()]['nb_view'] = $data['nb_view'];
            $datas['movie_'.$movie->getId()]['updated_date'] = $data['updated_date'];
            if(isset( $data['link']) && isset ($data['name'])){
                $datas['movie_'.$movie->getId()]['link'] = $data['link'];
                $datas['movie_'.$movie->getId()]['platform'] = $data['name'];
            }
        }
        
        return $datas;

    }

}