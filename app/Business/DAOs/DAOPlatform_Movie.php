<?php

namespace App\Business\DAOs;

use App\Business\DAOs\Value;
use App\Business\DAOs\ArrayValue;

class DAOPlatform_Movie extends DAOModel{

   /**
     * 
     * @param int $idPlatform
     * @param int $idMovie
     * @param string $link
     * @return bool
     */
    public function insert(int $idPlatform, int $idMovie , String $link) : bool
    {
        $sql = "
            INSERT INTO platform_movie (id_platform,id_movie,link)
            VALUES (:id_platform,:id_movie,:link);
        ";

        $values = new ArrayValue();
        $values->addValue(new Value(':id_platform', "".$idPlatform, 'int'));
        $values->addValue(new Value(':id_movie', "".$idMovie, 'int'));
        $values->addValue(new Value(':link', $link, 'string'));

        return $this->db->prepare($sql,$values,'insert') === 1;
    }

    /**
     * 
     * @param int $idPlatform
     * @param int $idMovie
     * @return string
     */
    public function getLink(int $idPlatform,int $idMovie) : string
    {
        $sql = "
            SELECT link
            FROM platform_movie
            WHERE id_platform = :id_platform AND id_movie = :id_movie
        ";

        $values = new ArrayValue();
        $values->addValue(new Value(':id_platform', "".$idPlatform, 'int'));
        $values->addValue(new Value(':id_movie', "".$idMovie, 'int'));

        return $this->db->prepare($sql,$values,'fetch')['link'];
    }

    /**
     * @param string $platform
     * @return bool
     */
    public function deleteLinkPlatform(string $platform): bool
    {
        $sql="
            DELETE FROM `platform_movie`
            INNER JOIN platform ON platform.name LIKE :platform
            WHERE id_platform = platform.id
        ";

        $values = new ArrayValue();
        $values->addValue(new Value(':platform', $platform, 'string'));

        return $this->db->prepare($sql,$values,'delete') >= 1;
    }

     /**
     * @param int $idPlatform
     * @param int $idMovie
     * @return bool
     */
    public function linkExist(int $idPlatform,int $idMovie): bool{
        $sql = "
            SELECT link
            FROM platform_movie
            WHERE id_platform = :id_platform AND id_movie = :id_movie
        ";

        $values = new ArrayValue();
        $values->addValue(new Value(':id_platform', "".$idPlatform, 'int'));
        $values->addValue(new Value(':id_movie', "".$idMovie, 'int'));

        return $this->db->prepare($sql,$values,'count') === 1;
    }

}