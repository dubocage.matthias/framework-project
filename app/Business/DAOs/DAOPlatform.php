<?php

namespace App\Business\DAOs;

use App\Business\Models\Platform;
use App\Business\DAOs\Value;
use App\Business\DAOs\ArrayValue;

class DAOPlatform extends DAOModel{

    /**
     * @return array
     */
    public function allPlatform(): array{

        $sql= " SELECT * FROM platform ";

        $platforms = [];
        
        foreach($this->db->query($sql) as $data){
            $platform = new Platform();
            $platform->hydrate($data);
            $platforms[] = $platform;
        }
        
        return $platforms;
    }

    /**
     * @param String $name
     * @return Platform
     */
    public function getPlatform(String $name): Platform{

        $sql= " 
            SELECT * 
            FROM platform
            WHERE name like :platform
        ";
        
        $values = new ArrayValue();
        $values->addValue(new Value(':platform', $name, 'string'));

        $platform = new Platform();
        $platform->hydrate($this->db->prepare($sql,$values,'fetch'));
        
        return $platform;
    }
}