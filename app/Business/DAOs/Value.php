<?php

namespace App\Business\DAOs;


class Value 
{
    private $param;
    private $value;
    private $type;

    function __construct(string $param, string $value ,string $type){
        $this->param = $param;
        $this->value = $value;
        $this->type = $type;
    }

    public function getParam() : string{
        return $this->param;
    }

    public function getValue() : string{
        return $this->value;
    }

    public function getType() : string{
        return $this->type;
    }

    public function setParam(string $param){
        return $this->param;
    }

    public function setValue(string $value){
        return $this->value;
    }

    public function setType(string $type){
        return $this->type;
    }
    
}