<?php

namespace App\Business\DAOs;

use App\Business\DAOs\Interfaces\DBConnectionInterface;

abstract class DAOModel
{
    protected $db;

    public function __construct(DBConnectionInterface $db)
    {
        $this->db = $db;
    }
}