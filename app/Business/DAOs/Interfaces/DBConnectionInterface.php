<?php

namespace App\Business\DAOs\Interfaces;

use App\Business\DAOs\ArrayValue;

interface DBConnectionInterface
{
    public function query(string $query, string $fetch = 'fetchAll');
    public function prepare(string $query, ArrayValue $values, string $fetch = 'fetchAll', bool $insert = false);
}