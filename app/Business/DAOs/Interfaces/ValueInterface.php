<?php

namespace App\Business\DAOs\Interfaces;

interface ValueInterface
{
    public function getParam();
    public function getValue();
    public function getType();
    public function setParam(string $param);
    public function setValue(string $value);
    public function setType(string $type);
}