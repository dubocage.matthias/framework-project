<?php

namespace App\Business\DAOs;

use App\Business\Models\Movie;
use App\Business\DAOs\Value;
use App\Business\DAOs\ArrayValue;
use DateTime;

class DAOMovie extends DAOModel{
    
    /**
     * La méthode de la DAO qui renvoie l'ensemble des films avec 
     * les informations du nombre de vue de l'utilisateur à sur le film
     * et de ca disponibiliter sur les différentes platforme.
     * @param int $pagination
     * @param int $nbMovie
     * @param string $search
     * @param string $username
     * @return array
     */
    public function allMovies(int $pagination = 0 , int $nbMovie = 10, string $search = '',string $username = '') : array 
    {

        $sql = "CALL allMovies(:nbMovie,:pagination,:search,:username)";

        $values = new ArrayValue();
        $values->addValue(new Value(':pagination',"".$pagination, 'int'));
        $values->addValue(new Value(':nbMovie', "".$nbMovie, 'int'));
        $values->addValue(new Value(':search','%'.$search.'%', $search === '' ? 'null': 'string'));
        $values->addValue(new Value(':username', $username,  'string'));

        $datas = [];
        foreach($this->db->prepare($sql,$values) as $data)
        {   
            $d = [];
            $movie = new Movie();
            $movie->hydrate($data);
            $d['movie'] = $movie;
            $d['nb_view'] = $data['nb_view'];
            $d['updated_date'] = $data['updated_date'];

            if(isset( $data['link']) && isset ($data['name']))
            {
                $d['link'] = $data['link'];
                $d['platform'] = $data['name'];
            }

            $datas[] = $d;
        }
        return $datas;
    }

    /**
     * La méthode de la DAO qui renvoie  le nombre de film contenue dans la recherche
     * @param string $search
     * @return int
     */
    public function nbMovie(string $search = '') : int {

        $sql = "
            SELECT COUNT(*) 
            FROM movie 
            WHERE title LIKE :search
        ";

        $values = new ArrayValue();
        $values->addValue(new Value(':search','%'.$search.'%','string'));

        return (int)$this->db->prepare($sql,$values,'fetch')['COUNT(*)'];
    }

    /**
     * La méthode de la DAO qui renvoie les informations 
     * du film correspondant à l'id passé en paramètre
     * @param int $id
     * @param string username
     * @return array
     */
    public function show(int $id,string $username = '') : array {

        $sql = "
            SELECT movie.*, user_movie.nb_view, user_movie.updated_date, platform.name, platform_movie.link
            FROM movie 
            LEFT JOIN user ON  user.username LIKE :username
            LEFT JOIN user_movie ON user_movie.id_movie = movie.id AND user_movie.id_user = user.id
            LEFT JOIN platform_movie ON platform_movie.id_movie = movie.id
            LEFT JOIN platform ON platform_movie.id_platform = platform.id
            WHERE movie.id = :id 
        ";

        $values = new ArrayValue();
        $values->addValue(new Value(':username', $username, 'string'));
        $values->addValue(new Value(':id', "".$id, 'int'));
        
        $data = $this->db->prepare($sql,$values,'fetch');
        $info = [];

        $movie = new Movie();
        $movie->hydrate($data);

        $info['movie'] = $movie;
        $info['view'] = $data['nb_view'];

        if(isset( $data['link']) && isset ($data['name'])) {
            $info['platform'] = $data['name'];
            $info['link'] = $data['link'];
        }

        return $info;
    }

    /**
     * La méthode de la DAO qui renvoie l'information de la présence 
     * du fim dans la bdd qui possède avec les valeurs title et releaseDate 
     * @param Movie $movie
     * @return bool
     */
    public function exist(Movie $movie):bool {
        $sql= "
            SELECT *
            FROM movie 
            WHERE title LIKE :title 
                AND releaseDate LIKE :releaseDate;
        ";
        $values = new ArrayValue();
        $values->addValue(new Value(':title', $movie->getTitle(), 'string'));
        $values->addValue(new Value(':releaseDate', $movie->getReleaseDate()->format("d/m/Y"),'string'));

        return $this->db->prepare($sql,$values,'count') >= 1 ;
    }

    /**
     * La méthode de la DAO qui renvoie un film avec la valeur aléatoire 
     * passé en paramètre
     * @param int $rand
     * @param string $username
     * @return array
     */
    public function randomMovie(int $rand, string $username = '') : array {
        $sql= "
            SELECT movie.*, user_movie.nb_view, user_movie.updated_date, platform.name, platform_movie.link
            FROM movie 
            LEFT JOIN user ON  user.username LIKE :username
            LEFT JOIN user_movie ON user_movie.id_movie = movie.id AND user_movie.id_user = user.id
            LEFT JOIN platform_movie ON platform_movie.id_movie = movie.id
            LEFT JOIN platform ON platform_movie.id_platform = platform.id
            LIMIT 1 OFFSET :random
        ";
        $values = new ArrayValue();
        $values->addValue(new Value(':random', "".$rand, 'int'));
        $values->addValue(new Value(':username', $username, 'string'));

        $data = $this->db->prepare($sql,$values,'fetch');
        $info = [];

        $movie = new Movie();
        $movie->hydrate($data);

        $info['movie'] = $movie;
        $info['view'] = $data['nb_view'];

        if(isset( $data['link']) && isset ($data['name'])){
            $info['platform'] = $data['name'];
            $info['link'] = $data['link'];
        }

        return  $info;
    }

    /**
     * La méthode de la DAO qui insére un film dans la bdd
     * @param Movie $movie
     * @return bool
     */
    public function insertMovie(Movie $movie) : bool {

        $sql= " INSERT INTO movie (title, releaseDate, duration, score, synopsis, poster) 
                VALUES (:title, :releaseDate, :duration, :score, :synopsis, :poster) ";

        $values = new ArrayValue();
        $values->addValue(new Value(':title', $movie->getTitle(), 'string'));
        $values->addValue(new Value(':releaseDate', $movie->getReleaseDate()->format("d/m/Y"), 'string'));
        $values->addValue(new Value(':duration', $movie->getDuration(), 'string'));
        $values->addValue(new Value(':synopsis', $movie->getScore(), 'string'));
        $values->addValue(new Value(':score', $movie->getSynopsis(), 'string'));
        $values->addValue(new Value(':poster', $movie->getPoster(), 'string'));

        return $this->db->prepare($sql,$values,'insert') === 1 ;
    }

    /**
     * La méthode de la DAO qui renvoi l'id du film possédent 
     * les même  valeur que titre et releaseDate passer en paramètre
     * @param String $title
     * @param String $releaseDate
     */
    public function getId(String $title ,String $releaseDate) : int {
        
        // LIMIT 1 au cas ou il y en ai plusieur
        $sql= "
            SELECT id 
            FROM movie 
            WHERE title LIKE :title 
                AND releaseDate LIKE :releaseDate;
            LIMIT 1
        ";

        $date = new DateTime($releaseDate);

        $values = new ArrayValue();
        $values->addValue(new Value(':title', $title, 'string'));
        $values->addValue(new Value(':releaseDate', $date->format("d/m/Y"), 'string'));

        return  $this->db->prepare($sql,$values,'fetch')['id'];
    }
}
