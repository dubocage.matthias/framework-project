<?php

namespace App\Services;
use Library\HTML_Dome_Parser\simple_html_dom;

class ParseWeb
{

    /**
     * @param String $title 
     * @return String $link
     */
    public static function getMovieLinkPlatforme(String $title, String $platforme): String
    {
        $title = str_replace(" " ,"+", $title);
        $title = str_replace("#" ,"%23", $title);
        $title = str_replace("l&%23039;" ,"'", $title);
        $title = str_replace('&','%26',$title);
        $title = str_replace('%','%25',$title);
        $url = "https://www.google.com/search?q=".$title."+".$platforme."+fr&hl=fr";
        $html = new simple_html_dom();
        $html->load_file($url);
        $collection = $html->find('div[class=ZINbbc xpd O9g5cc uUPGi] div[class=kCrYT]');
        foreach($collection as $element){
            if($element->children(0)->href ){
                $link = explode('q=',explode('&',$element->children(0)->href)[0])[1];
                if( strpos($link, 'https://www.'.$platforme.'.com/') !== false ){
                    return $link;
                }
            }
        }
        return "";
    }

}

?>