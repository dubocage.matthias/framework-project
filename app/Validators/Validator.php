<?php

namespace App\Validators;

class Validator
{
    private $data;
    private $error;

    public function __construct(array $data)
    {
        $this->error = [];
        $this->data = $data;
    }

    /**
     * La mèthode va téster chaque règle lié aux données que contient la class
     * @param array $rules
     * @return array
     */
    public function validate(array $rules) : array
    {
        foreach($rules as $name => $rulesArray)
        {
            if(array_key_exists($name,$this->data))
            {
                foreach($rulesArray as $rule)
                {
                    switch($rule)
                    {
                        case 'required':
                            $this->required($name,$this->data[$name]);
                            break;
                        case 'username':
                            $this->username($name,$this->data[$name]);
                            break;
                        case 'password':
                            $this->password($name,$this->data[$name]);
                            break;
                        case 'mail':
                            $this->mail($name,$this->data[$name]);
                            break;
                        case 'id':
                            $this->id($name,$this->data[$name]);
                            break;
                        case 'numChar':
                            $this->numChar($name,$this->data[$name]);
                            break; 
                        default :
                            // ------
                            break;
                    }
                }
            }
        }

        return $this->error;
    }


    /**
     * La méthode teste si la donnée existe
     * @param string $name
     * @param string $value
     * @return void
     */
    private function required(string $name, string $value): void
    {
        $value = trim($value);

        if(!isset($value) || empty($value) || is_null($value)){
            $this->error[$name]= str_replace('{$name}', $name,VALIDATOR_ERROR_REQUIRED);
        }
    } 


    /**
     * La méthode test si la donnée respecte la regex lié au nom de l'utilisateur 
     * @param string $name
     * @param string $value
     * @return void
     */
    private function username(string $name, string $value): void
    {
        if (!preg_match('/^[a-zA-Z\d_]{5,20}$/', $value))
        {
            $this->error[$name]= VALIDATOR_ERROR_USERNAME;
        }
    }


    /**
     * La méthode test si la donnée respecte la regex lié au mot de passe
     * @param string $name
     * @param string $value
     * @return void
     */
    private function password(string $name, string $value): void
    {
        if (!preg_match('/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[%#:$*!])[0-9A-Za-z%#:$*!]{16,32}$/', $value))
        {
            $this->error[$name]= VALIDATOR_ERROR_PASSWORD;
        }
    }

    /**
     * La méthode test si la donnée respecte la regex lié à l'adresse mail
     * @param string $name
     * @param string $value
     * @param void
     */
    private function mail(string $name, string $value): void
    {
        if (!filter_var($value, FILTER_VALIDATE_EMAIL))
        {
            $this->error[$name]= VALIDATOR_ERROR_MAIL;
        }
    }

    /**
     * La méthode test si la donnée respecte la regex lié à l'id
     * @param string $name
     * @param string $value
     * @param void
     */
    private function id(string $name, string $value): void
    {
        if (!preg_match("/^[1-9]+[0-9]*$/", $value))
        {
            $this->error[$name]= str_replace('{$name}', $name , VALIDATOR_ERROR_IDVALUE);
        }
    }

    /**
     * La méthode test si la donnée respecte la regex au chaine de caractère qui contient des chiffres
     * @param string $name
     * @param string $value
     * @param void
     */
    private function numChar(string $name, string $value): void
    {
        if (!preg_match("/^[0-9a-z-A-Z\s]*$/", $value))
        {
            $this->error[$name]= str_replace('{$name}', $name , VALIDATOR_ERROR_IDVALUE);
        }
    }
}