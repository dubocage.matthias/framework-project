<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class RequestDBException extends Exception
{
    public function __construct($message = "", $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message,$code,$previous);
        http_response_code(500);
        require VIEWS . 'errors'. DIRECTORY_SEPARATOR.'errorServer.php'; 
        die();
    }

}