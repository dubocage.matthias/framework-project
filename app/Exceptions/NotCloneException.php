<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class NotCloneException extends Exception
{
    public function __construct($message = "", $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message,$code,$previous);
    }

    public function errorClone()
    {
        http_response_code(500);
        require VIEWS . 'errors'. DIRECTORY_SEPARATOR.'errorServer.php'; 
        die();
    }

}