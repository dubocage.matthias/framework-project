<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>API Movies</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= SCRIPTS .''.DIRECTORY_SEPARATOR .'css'. DIRECTORY_SEPARATOR . 'app.css'?>">
    <link rel="icon" type="image/png" href="<?= IMAGES . DIRECTORY_SEPARATOR ?>images/favicon.png" > 
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script src="<?= SCRIPTS .''.DIRECTORY_SEPARATOR .'script'. DIRECTORY_SEPARATOR . 'alerts.js'?>"></script>
    <script src="<?= SCRIPTS .''.DIRECTORY_SEPARATOR .'script'. DIRECTORY_SEPARATOR . 'affichage.js'?>"></script>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <a class="navbar-brand" href="/">API Movies</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="/movies"><?= HTML_LAYOUT_LINK_MOVIE ?></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/random"><?= HTML_LAYOUT_LINK_RANDOM ?></a>
                        </li>
                        <?php if(isset($_SESSION['token'])){?>
                        <li class="nav-item">
                            <a class="nav-link" href="/view/<?= $_SESSION['username']?>"><?= HTML_LAYOUT_LINK_MOVIESVIEW ?></a>
                        </li>
                        <?php } ?>
                        <li class="nav-item">
                            <a class="nav-link disabled" href="#">API</a>
                        </li>
                    </ul>
                <div>
                <?php if(isset($_SESSION['token'])){?>
                    <a class="btn btn-outline-danger my-2 my-sm-0" href="/deconnection"><?= HTML_LAYOUT_INPUT_LOGOUT ?></a>
                <?php }else{?>
                    <a class="btn btn-outline-primary my-2 my-sm-0" href="/connection"><?= HTML_LAYOUT_INPUT_LOGIN ?></a>
                <?php } ?>
            </div>
            </div>
        </div>
    </nav>
    <div class="container" style="margin-top:30px;">
        <?php  if(isset($_SESSION['errors'])) { ?>
            <div id="errors" class="section">
                <div class="alert alert-danger center" role="alert" >
                    <?php foreach($_SESSION['errors'] as $message){ ?>
                        <?= $message ?>
                        <br>
                    <?php }?>
                </div> 
            </div> 
            <?php unset($_SESSION['errors']); ?> 
        <?php   }
        if (isset($_SESSION['validations'])) {?>
            <div id="validations" class="section">
                <div  class="alert alert-success center" style ="text-align:center" role="alert" >
                    <?php foreach($_SESSION['validations'] as $message){ ?>
                        <?= $message ?>
                        <br>
                    <?php }?>
                </div>
            </div>
            <?php unset($_SESSION['validations']); ?> 
        <?php   } ?> 
        <div id="collaspsed_content">          
            <?= $content ?>
        </div>
    </div>
</body>
<script>
    alert();
    collaspsed_Content();
</script>
</html>