<div class="text-center">
    <h1><?= HTML_PROFILE_VIEW_TITLE . $params['data']['username']?></h1>
</div>

<form class="form-inline" methode='GET' action="/view/<?= $params['data']['username']?>">
    <input class="form-control mr-sm-2" type="text" placeholder="<?= HTML_PROFILE_VIEW_INPUT_PLACEHOLDER_SEARCH ?>" name="search" value="<?php if(isset($params['data']['search'])){ echo $params['data']['search'];}?>">
    <button class="btn btn-outline-primary my-2 my-sm-0" type="submit"><?= HTML_PROFILE_VIEW_INPUT_SUBMIT_SEARCH ?></button>
</form>

<?php require VIEWS . 'template\listMovie.php';?>