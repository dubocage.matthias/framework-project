<div class="text-center">
    <h1><?= $params['data']['movie']->getTitle(); ?></h1>
</div>
<div class="center">
    <img class="img-movie" src="<?= $params['data']['movie']->getPoster() ?>">
</div>
<div class="column">
    <h5 class="center"><b><?= HTML_MOVIE_SHOW_RELEASE_DATE ?></b> : <?= $params['data']['movie']->getReleaseDate()->format("d/m/Y") ?></h5>
    <h5 class="center"><b><?= HTML_MOVIE_SHOW_DURATION ?></b> : <?= $params['data']['movie']->getDuration() ?> mn</h5>
    <?php require VIEWS . 'template\buttonPlatform.php';?>
    <div class="center column">
        <h5><b><?= HTML_MOVIE_SHOW_SYNOPSIS ?></b> :</h5>
        <p><?= $params['data']['movie']->getSynopsis() ?></p>
    </div>
    <form  class='center' action="/addView" method="post">
        <input type="hidden" name="id" value="<?= $params['data']['movie']->getId();?>">
        <button type="submit" class="btn btn-success view-button"><?= HTML_MOVIE_SHOW_VIEW ?> <?php if($params['data']['view'] > 0){echo $params['data']['view'].  HTML_TEMPLATE_MOVIELIST_INPUT_VIEW_TIMES;}?></button>
    </form>
</div>