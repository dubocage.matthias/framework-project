
<div class="text-center">
    <h1><?= HTML_MOVIE_ALL_TITLE ?></h1>
</div>

<form class="form-inline" methode='GET' action="movies">
    <input class="form-control mr-sm-2" type="text" placeholder="<?= HTML_MOVIE_ALL_INPUT_PLACEHOLDER_SEARCH ?>" name="search" value="<?php if(isset($params['data']['search'])){ echo $params['data']['search'];}?>">
    <button class="btn btn-outline-primary my-2 my-sm-0" type="submit"><?= HTML_MOVIE_ALL_INPUT_SUBMIT_SEARCH ?></button>
</form>

<?php require VIEWS . 'template\listMovie.php';?>
