<?php if(isset($movie['link']) && isset($movie['platform'])) { ?>
    <a  href="<?= $movie['link']?>">
        <img class="imgButtonPlatform" src="<?= IMAGES . DIRECTORY_SEPARATOR ?>images/button_<?=$movie['platform']?>.png">
    </a>
<?php } ?>

<?php if(isset($params['data']['link']) && isset($params['data']['platform'])) { ?>
    <a  class='center' href="<?= $params['data']['link']?>">
        <img class="imgButtonPlatform" src="<?= IMAGES . DIRECTORY_SEPARATOR ?>images/button_<?=$params['data']['platform']?>.png">
    </a>
<?php } ?>