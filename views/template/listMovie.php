<?php require VIEWS . 'template\pagination.php';?>
<div class="list-movie">
    <?php foreach($params['data']['movies'] as $movie){ ?>
        <div class="movie" style="padding-bottom:20px;">
                <div class="img-movie">
                    <a  href="/movie/<?= $movie['movie']->getId() ?>">
                        <img class="poster" src="<?= $movie['movie']->getPoster() ?>">
                    </a>
                    <form action="/addView" method="post">
                        <input type="hidden" name="id" value="<?= $movie['movie']->getId();?>">
                        <button type="submit" class="btn btn-success view-button"><?= HTML_TEMPLATE_MOVIELIST_INPUT_VIEW ?> <?php if($movie['nb_view'] > 0){echo $movie['nb_view'].HTML_TEMPLATE_MOVIELIST_INPUT_VIEW_TIMES ;}?></button>
                    </form>
                </div>
                <div class="information-movie">
                    <div>
                        <a  href="/movie/<?= $movie['movie']->getId() ?>">
                            <h2><?= $movie['movie']->getTitle() ?></h2>
                        </a>
                    </div>
                    <div>
                        <h5><b><?= HTML_TEMPLATE_MOVIELIST_RELEASE_DATE ?></b> : <?= $movie['movie']->getReleaseDate()->format("d/m/Y") ?></h5>
                    </div>
                    <div>
                        <h5><b><?= HTML_TEMPLATE_MOVIELIST_DURATION ?></b> : <?= $movie['movie']->getDuration() ?> mn</h5>
                    </div>
                    <div>
                        <h5><b><?= HTML_TEMPLATE_MOVIELIST_SYNOPSIS ?></b> :</h5>
                        <p><?= $movie['movie']->getSynopsis() ?></p>
                    </div>
                    <div>
                        <?php require VIEWS . 'template\buttonPlatform.php';?>
                    </div>
                </div>
            </div>
    <?php } ?>
</div>
<?php require VIEWS . 'template\pagination.php';?>