<?php if($params['data']['nbPage'] !== 0 ){ ?>
    <hr>
    <div class="pagination center">
        <?php if($params['data']['page'] > 1 ){ ?>
            <?php if($params['data']['page']-1 > 1 ){ ?>
            <a href="?page=<?=$params['data']['page']-1?><?php if(isset($params['data']['search'])){?>&search=<?=$params['data']['search'];}?>"><<</a>
            <?php } ?>
            <a href="?page=1<?php if(isset($params['data']['search'])){?>&search=<?=$params['data']['search'];}?>">1</a>
            <?php if($params['data']['page']-1 > 1 ){ ?>
                <div>...</div>
            <?php } ?>
        <?php } ?>
        <div><?= $params['data']['page']?></div>
        <?php if($params['data']['page'] < $params['data']['nbPage'] ){ ?>
            <?php if($params['data']['page']+1 < $params['data']['nbPage'] ){ ?>
                <div>...</div>
            <?php } ?>
            <a href="?page=<?=$params['data']['nbPage']?><?php if(isset($params['data']['search'])){?>&search=<?=$params['data']['search'];}?>"><?=$params['data']['nbPage']?></a>
            <?php if($params['data']['page']+1 < $params['data']['nbPage'] ){ ?>
                <a href="?page=<?=$params['data']['page']+1?><?php if(isset($params['data']['search'])){?>&search=<?=$params['data']['search'];}?>">>></a>
            <?php } ?>
        <?php }?>
    </div>
    <hr>
<?php }?>