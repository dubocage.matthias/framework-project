<div class="center">
  <div class="column w-50">
    <h1 class="mt-3 mb-5 "><?= HTML_CONNECTION_TITLE_LOGIN ?></h1>
    <form action="/connection" method="POST">

      <div class="form-group mt-4">
        <label for="username"><?= HTML_CONNECTION_LABEL_USERNAME ?></label>
        <input type="text" class="form-control" id="username" name="username" placeholder="<?= HTML_CONNECTION_INPUT_PLACEHOLDER_USERNAME ?>" <?php if(isset($params['data']['username'])){ echo "value=\"".$params['data']['username']."\""; }?> required >
      </div>

      <div class="form-group mt-4">
        <label for="password"><?= HTML_CONNECTION_LABEL_PASSWORD ?></label>
        <input type="password" class="form-control" id="password" name="password" placeholder="<?= HTML_CONNECTION_INPUT_PLACEHOLDER_PASSWORD ?>" <?php if(isset($params['data']['password'])){ echo "value=\"".$params['data']['password']."\""; }?> required >
      </div>

      <div class="center mt-5 ">
        <button type="submit" class="btn btn-primary mr-2 "><?= HTML_CONNECTION_INPUT_SUBMIT ?></button>
        <a class="btn btn-secondary my-2 my-sm-0" href="/registration"><?= HTML_CONNECTION_INPUT_INSCRIPTION ?></a>
      </div>
    </form>
  </div>
</div>
