<div class="center">
  <div class="column w-50">
    <h1 class="mt-3 mb-5"><?= HTML_REGISTRATION_TITLE ?></h1>
    <form action="/registration" method="POST">

      <div class="form-group mt-4">
        <label for="username"><?= HTML_REGISTRATION_INPUT_LABEL_USERNAME ?></label>
        <input type="text" class="form-control" id="username" name="username" placeholder="<?= HTML_REGISTRATION_INPUT_PLACEHOLDER_USERNAME ?>"  <?php if(isset($params['data']['username'])){ echo "value=\"".$params['data']['username']."\""; }?> required >
      </div>

      <div class="form-group mt-4">
        <label for="mail"><?= HTML_REGISTRATION_INPUT_LABEL_MAIL ?></label>
        <input type="mail" class="form-control" id="mail" name="mail" placeholder="<?= HTML_REGISTRATION_INPUT_PLACEHOLDER_MAIL ?>" <?php if(isset($params['data']['mail'])){ echo "value=\"".$params['data']['mail']."\""; }?> required >
      </div>

      <div class="form-group mt-4">
        <label for="password"><?= HTML_REGISTRATION_INPUT_LABEL_PASSWORD ?></label>
        <input type="password" class="form-control" id="password" name="password" placeholder="<?= HTML_REGISTRATION_INPUT_PLACEHOLDER_PASSWORD ?>"  <?php if(isset($params['data']['password'])){ echo "value=\"".$params['data']['password']."\""; }?> required >
      </div>

      <div class="form-group mt-3">
        <input type="password" class="form-control" id="passwordAgain" name="passwordAgain" placeholder="<?= HTML_REGISTRATION_INPUT_PLACEHOLDER_PASSWORDAGAIN ?>" <?php if(isset($params['data']['passwordAgain'])){ echo "value=\"".$params['data']['passwordAgain']."\""; }?>  required>
      </div>

      <div class="center mt-5">
        <button type="submit" class="btn btn-primary"> <?= HTML_REGISTRATION_INPUT_SUBMIT ?> </button>
      </div>
      
    </form>
  </div>
</div>