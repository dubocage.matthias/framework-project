<?php 
namespace Tests\Business;

use App\Business\MovieBusiness;
use Database\DBConnection;
use PHPUnit\Framework\TestCase;

class MovieBusinessTest extends TestCase
{   
    public function testNbPageKey(){
        $movieBusiness = new MovieBusiness(DBConnection::getInstance());
        $this->assertArrayHasKey('nbPage',$movieBusiness->getNbPage());
    }

    public function testAllMovies(){
        $movieBusiness = new MovieBusiness(DBConnection::getInstance());
        $this->assertArrayHasKey('movies',$movieBusiness->allMovies());
        $this->assertArrayHasKey('page',$movieBusiness->allMovies());
        $this->assertArrayHasKey('nbPage',$movieBusiness->allMovies());
    }
}