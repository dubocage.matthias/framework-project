<?php 
namespace Tests\Business\DAOs;

use App\Business\DAOs\DAOMovie;
use Database\DBConnection;
use PHPUnit\Framework\TestCase;

class DAOMovieTest extends TestCase
{   
    public function testNbPage(){
        $daoMovie = new DAOMovie(DBConnection::getInstance());
        $this->assertTrue(is_int($daoMovie->nbMovie()));
    }

    public function testAllMovies(){
        $daoMovie = new DAOMovie(DBConnection::getInstance());
        $this->assertTrue(is_array($daoMovie->allMovies()));
        $this->assertTrue(is_array($daoMovie->allMovies()[]));
    }
}