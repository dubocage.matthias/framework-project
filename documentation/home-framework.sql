-- --------------------------------------------------------
-- Hôte :                        localhost
-- Version du serveur:           5.7.24 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             9.5.0.5332
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour home_framework
CREATE DATABASE IF NOT EXISTS `home_framework` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `home_framework`;

-- Listage de la structure de la table home_framework. actor_movie
CREATE TABLE IF NOT EXISTS `actor_movie` (
  `id_character` int(11) NOT NULL,
  `id_movie` int(11) NOT NULL,
  KEY `FK_ID_CHARACTER_ACTOR_idx` (`id_character`),
  KEY `FK_ID_MOVIE_ACTOR_idx` (`id_movie`),
  CONSTRAINT `FK_ID_CHARACTER_ACTOR` FOREIGN KEY (`id_character`) REFERENCES `character` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_ID_MOVIE_ACTOR` FOREIGN KEY (`id_movie`) REFERENCES `movie` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Listage de la structure de la table home_framework. character
CREATE TABLE IF NOT EXISTS `character` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `biography` longtext,
  `photo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Listage de la structure de la table home_framework. comment
CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_movie` int(11) DEFAULT NULL,
  `id_comment` int(11) DEFAULT NULL,
  `message` longtext NOT NULL,
  `like` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_comment_UNIQUE` (`id`),
  KEY `FK_ID_MOVIE_COMMENT_idx` (`id_movie`),
  KEY `FK_ID_USER_COMMENT_idx` (`id_user`),
  KEY `FK_ID_COMMENT_COMMENT_idx` (`id_comment`),
  CONSTRAINT `FK_ID_COMMENT_COMMENT` FOREIGN KEY (`id_comment`) REFERENCES `comment` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_ID_MOVIE_COMMENT` FOREIGN KEY (`id_movie`) REFERENCES `movie` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_ID_USER_COMMENT` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Listage de la structure de l'évènement home_framework. delete_notification_deactivate
DELIMITER //
CREATE DEFINER=`root`@`localhost` EVENT `delete_notification_deactivate` ON SCHEDULE EVERY 1 DAY STARTS '2021-02-28 19:43:52' ON COMPLETION NOT PRESERVE ENABLE DO DELETE FROM notification 
	WHERE viewing = 'deactivated' AND NOW() > DATE_ADD(update_at,INTERVAL 1 day)//
DELIMITER ;

-- Listage de la structure de la table home_framework. director_movie
CREATE TABLE IF NOT EXISTS `director_movie` (
  `id_character` int(11) NOT NULL,
  `id_movie` int(11) NOT NULL,
  KEY `FK_ID_CHARACTER_DIRECTOR_idx` (`id_character`),
  KEY `FK_ID_MOVIE_DIRECTOR_idx` (`id_movie`),
  CONSTRAINT `FK_ID_CHARACTER_DIRECTOR` FOREIGN KEY (`id_character`) REFERENCES `character` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_ID_MOVIE_DIRECTOR` FOREIGN KEY (`id_movie`) REFERENCES `movie` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Listage de la structure de la table home_framework. friend
CREATE TABLE IF NOT EXISTS `friend` (
  `id_user1` int(11) NOT NULL,
  `id_user2` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  UNIQUE KEY `FK_ID_USER1_ID_USER2_UNIQUE` (`id_user1`,`id_user2`),
  KEY `FK_ID_USER1_idx` (`id_user1`),
  KEY `FK_ID_USER2_idx` (`id_user2`),
  CONSTRAINT `FK_ID_USER1` FOREIGN KEY (`id_user1`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_ID_USER2` FOREIGN KEY (`id_user2`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Listage de la structure de la table home_framework. invitation
CREATE TABLE IF NOT EXISTS `invitation` (
  `id_user1` int(11) NOT NULL,
  `id_user2` int(11) NOT NULL,
  `reponse` enum('accept','refuse','waiting') DEFAULT 'waiting',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `FK_ID_USER1_ID_USER2_UNIQUE` (`id_user1`,`id_user2`),
  KEY `FK_ID_USER1_INVITATION_idx` (`id_user1`),
  KEY `FK_ID_USER2_INVITATION_idx` (`id_user2`),
  CONSTRAINT `FK_ID_USER1_INVITATION` FOREIGN KEY (`id_user1`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_ID_USER2_INVITATION` FOREIGN KEY (`id_user2`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Listage de la structure de la table home_framework. movie
CREATE TABLE IF NOT EXISTS `movie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `releaseDate` varchar(255) DEFAULT NULL,
  `poster` varchar(300) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `score` varchar(255) NOT NULL DEFAULT '0.0',
  `synopsis` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34598 DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Listage de la structure de la procédure home_framework. nbMovies
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `nbMovies`(
	IN `in_search` VARCHAR(255),
	OUT `nbMovies` INT
)
BEGIN
	SELECT COUNT(*) FROM movie WHERE title LIKE in_search;
END//
DELIMITER ;

-- Listage de la structure de la table home_framework. notification
CREATE TABLE IF NOT EXISTS `notification` (
  `id_user` int(11) NOT NULL,
  `message` longtext NOT NULL,
  `viewing` enum('actived','deactivated') DEFAULT 'actived',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` datetime DEFAULT NULL,
  KEY `FK_ID_USER_NOTIFICATION_idx` (`id_user`),
  CONSTRAINT `FK_ID_USER_NOTIFICATION` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Listage de la structure de la table home_framework. platform
CREATE TABLE IF NOT EXISTS `platform` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Listage de la structure de la table home_framework. platform_movie
CREATE TABLE IF NOT EXISTS `platform_movie` (
  `id_platform` int(11) NOT NULL,
  `id_movie` int(11) NOT NULL,
  `link` varchar(500) NOT NULL,
  UNIQUE KEY `FK_ID_MOVIE_ID_PLATFORM` (`id_platform`,`id_movie`),
  KEY `FK_ID_PLATFORM_idx` (`id_platform`),
  KEY `FK_ID_MOVIE_PLATFORM_idx` (`id_movie`),
  CONSTRAINT `FK_ID_MOVIE_PLATFORM` FOREIGN KEY (`id_movie`) REFERENCES `movie` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_ID_PLATFORM` FOREIGN KEY (`id_platform`) REFERENCES `platform` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Listage de la structure de la table home_framework. user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL DEFAULT '0',
  `password` varchar(512) NOT NULL DEFAULT '0',
  `update_token` datetime DEFAULT NULL,
  `token` varchar(500) DEFAULT NULL,
  `salt` varchar(512) DEFAULT NULL,
  `mail` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Listage de la structure de la table home_framework. user_movie
CREATE TABLE IF NOT EXISTS `user_movie` (
  `id_user` int(11) NOT NULL,
  `id_movie` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime DEFAULT NULL,
  `nb_view` int(11) DEFAULT '1',
  UNIQUE KEY `id_movie_user_unique` (`id_user`,`id_movie`),
  KEY `id_user` (`id_user`),
  KEY `id_movie` (`id_movie`),
  CONSTRAINT `FK_ID_MOVIE` FOREIGN KEY (`id_movie`) REFERENCES `movie` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_ID_USER` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Listage de la structure de déclencheur home_framework. accepted_invitation_notification
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `accepted_invitation_notification` AFTER UPDATE ON `invitation` FOR EACH ROW BEGIN
	IF NEW.reponse = 'accept' THEN
		INSERT INTO notification (id_user,message) VALUES (OLD.id_user1,'Validation de votre invitation!');
    END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Listage de la structure de déclencheur home_framework. add_notification_user
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `add_notification_user` AFTER INSERT ON `comment` FOR EACH ROW BEGIN
	DECLARE m_user_comment_id integer;
	IF NEW.id_comment IS NOT NULL THEN
		SET @m_user_comment_id = (SELECT `id_user` FROM `comment` WHERE id = NEW.id_comment);
        INSERT INTO notification(id_user,message) VALUES(@m_user_comment_id,concat('Vous avez resus un nouveau message: ',NEW.message));
    END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Listage de la structure de déclencheur home_framework. waiting_invitation_notification
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `waiting_invitation_notification` AFTER INSERT ON `invitation` FOR EACH ROW BEGIN
	INSERT INTO notification (id_user,message) VALUES (NEW.id_user2,'Nouvelle invitation !');
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
