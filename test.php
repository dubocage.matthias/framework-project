<?php
require 'vendor/autoload.php';

use Library\HTML_Dome_Parser\simple_html_dom;
use App\Services\ParseWeb;
use App\Business\MovieBusiness;
use App\Business\Platform_MovieBusiness;
use Database\DBConnection;
use Logger\LoggerLevel;
use Logger\Logger;


$platform_MovieBusiness = new Platform_MovieBusiness(DBConnection::getInstance());
$movieBusiness = new MovieBusiness(DBConnection::getInstance());

while( $page <= 214){
    primevideo2($page, $platform_MovieBusiness , $movieBusiness);
    $page ++;
}



function netflix(Platform_MovieBusiness $plat,MovieBusiness $movie){
    if(!isset($_GET['page']))
    {
        $page = 1;
        $data = $plat->deleteLinkPlatform('netflix');
        if(isset($data['validation'])){
            Logger::addLog($data['validation'],LoggerLevel::INFO,$_SERVER['REMOTE_ADDR']);
        }else{
            Logger::addLog($data['error'],LoggerLevel::WARNING,$_SERVER['REMOTE_ADDR']);
        }
    }else{
        $page = $_GET['page'];
    }

    if( $page <= 104 ){
        $month =    ['janvier' => '01' , 'février' => '02' , 'mars' => '03', 'avril' => '04', 'mai' => '05', 'juin' => '06', 
                        'juillet' => '07','août'=> '08' ,'septembre'=> '09','octobre'=> '10','novembre' => '11' ,'décembre'=>'12'];
        try {
            $url = "https://www.allocine.fr/netflix/film/tous/?page=" . $page;
            $html = new simple_html_dom();
            $html->load_file($url);
            $collection = $html->find('li[class=mdl] div[class=card entity-card entity-card-list cf]');
            foreach($collection as $element){
                $title = $element->children(1)->children(0)->children(0)->plaintext;
                if($element->children(1)->children(1)->children(1) != null){
                    $textdate = explode(' ',$element->children(1)->children(1)->children(0)->children(0)->plaintext) ;

                    if(sizeof($textdate) == 3 &&  ($textdate[1] == 'janvier' || $textdate[1] == 'février' || $textdate[1] == 'mars'|| $textdate[1] ==  'avril'|| $textdate[1] == 'mai'|| $textdate[1] ==  'juin' 
                    || $textdate[1] == 'juillet'|| $textdate[1] == 'août'|| $textdate[1] == 'septembre'|| $textdate[1] == 'octobre'|| $textdate[1] == 'novembre'|| $textdate[1] == 'décembre')){
                        $year = $textdate[2] . "-" . $month[$textdate[1]] . "-" . $textdate[0];
                    }
                    
                    $title = str_replace("&#039;" ,"'", $title);
                    $title = str_replace("&amp;" ,"&", $title);
                    if(isset($year)){
                        if($movie->movieExiste($title,$year)){
                            $link = ParseWeb::getMovieLinkPlatforme($title,'netflix');
                            sleep(2);
                            $data = $plat->addPlatform_Movie($title,$year,'netflix',$link);
                            if(isset($data['validation'])){
                                Logger::addLog($data['validation'],LoggerLevel::INFO,$_SERVER['REMOTE_ADDR']);
                            }else{
                                Logger::addLog($data['error'],LoggerLevel::WARNING,$_SERVER['REMOTE_ADDR']);
                            }
                        }else{
                            Logger::addLog("Le film ".$title." avec la date ".$year." n'a pas été trouvé dans la bdd" ,LoggerLevel::INFO,$_SERVER['REMOTE_ADDR']);
                        }
                    }else{
                        Logger::addLog("Le film ".$title." n'a pas de date" ,LoggerLevel::INFO,$_SERVER['REMOTE_ADDR']);
                    }
                }
            }
        } catch (Exception $e) {
            var_dump("OUPS l'ouverture de la page ".$page." a échoué.<br>");
        }
        return header('Location: /add/netflix&page='.( $page+1 ));
    }else{
        var_dump("FIN du programe !");
    }
}


 function primevideo(Platform_MovieBusiness $plat,MovieBusiness $movie){

    if(!isset($_GET['page']))
    {
        $page = 1;
        $data = $plat->deleteLinkPlatform('primevideo');
        if(isset($data['validation'])){
            Logger::addLog($data['validation'],LoggerLevel::INFO,$_SERVER['REMOTE_ADDR']);
        }else{
            Logger::addLog($data['error'],LoggerLevel::WARNING,$_SERVER['REMOTE_ADDR']);
        }
    }else{
        $page = $_GET['page'];
    }

    if( $page <= 214 ){
        $month =    ['janvier' => '01' , 'février' => '02' , 'mars' => '03', 'avril' => '04', 'mai' => '05', 'juin' => '06', 
                        'juillet' => '07','août'=> '08' ,'septembre'=> '09','octobre'=> '10','novembre' => '11' ,'décembre'=>'12'];
        try {
            $url = "https://www.allocine.fr/amazon/film/tous/?page=" . $page;
            $html = new simple_html_dom();
            $html->load_file($url);
            $collection = $html->find('li[class=mdl] div[class=card entity-card entity-card-list cf]');
            foreach($collection as $element){
                $title = $element->children(1)->children(0)->children(0)->plaintext;
                if($element->children(1)->children(1)->children(1) != null){
                    $textdate = explode(' ',$element->children(1)->children(1)->children(0)->children(0)->plaintext) ;

                    if(sizeof($textdate) == 3 &&  ($textdate[1] == 'janvier' || $textdate[1] == 'février' || $textdate[1] == 'mars'|| $textdate[1] ==  'avril'|| $textdate[1] == 'mai'|| $textdate[1] ==  'juin' 
                    || $textdate[1] == 'juillet'|| $textdate[1] == 'août'|| $textdate[1] == 'septembre'|| $textdate[1] == 'octobre'|| $textdate[1] == 'novembre'|| $textdate[1] == 'décembre')){
                        $year = $textdate[2] . "-" . $month[$textdate[1]] . "-" . $textdate[0];
                    }
                    
                    $title = str_replace("&#039;" ,"'", $title);
                    $title = str_replace("&amp;" ,"&", $title);

                    if($this->movieBusiness->movieExiste($title,$year)){
                        $link = ParseWeb::getMovieLinkPlatforme($title,'primevideo');
                        sleep(2);
                        $data = $this->platform_MovieBusiness->addPlatform_Movie($title,$year,'primevideo',$link);
                        if(isset($data['validation'])){
                            Logger::addLog($data['validation'],LoggerLevel::INFO,$_SERVER['REMOTE_ADDR']);
                        }else{
                            Logger::addLog($data['error'],LoggerLevel::WARNING,$_SERVER['REMOTE_ADDR']);
                        }
                    }else{
                        Logger::addLog("Le film ".$title." avec la date ".$year." n'a pas été trouvé dans la bdd" ,LoggerLevel::INFO,$_SERVER['REMOTE_ADDR']);
                    }
                }
            }
        } catch (Exception $e) {
            var_dump("OUPS l'ouverture de la page ".$page." a échoué.<br>");
        }
        return header('Location: /add/primevideo&page='.( $page+1 ));
    }else{
        var_dump("FIN du programe !");
    }
}

function disney(Platform_MovieBusiness $plat,MovieBusiness $movie){

    if(!isset($_GET['page']))
    {
        $page = 1;
        $data = $plat->deleteLinkPlatform('disneyplus');
        if(isset($data['validation'])){
            Logger::addLog($data['validation'],LoggerLevel::INFO,$_SERVER['REMOTE_ADDR']);
        }else{
            Logger::addLog($data['error'],LoggerLevel::WARNING,$_SERVER['REMOTE_ADDR']);
        }
    }else{
        $page = $_GET['page'];
    }

    if( $page <= 62 ){
        $month =    ['janvier' => '01' , 'février' => '02' , 'mars' => '03', 'avril' => '04', 'mai' => '05', 'juin' => '06', 
                        'juillet' => '07','août'=> '08' ,'septembre'=> '09','octobre'=> '10','novembre' => '11' ,'décembre'=>'12'];
        try {
            $url = "https://www.allocine.fr/disney/film/tous/?page=" . $page;
            $html = new simple_html_dom();
            $html->load_file($url);
            $collection = $html->find('li[class=mdl] div[class=card entity-card entity-card-list cf]');
            foreach($collection as $element){
                $title = $element->children(1)->children(0)->children(0)->plaintext;
                if($element->children(1)->children(1)->children(1) != null){
                    $textdate = explode(' ',$element->children(1)->children(1)->children(0)->children(0)->plaintext) ;

                    if(sizeof($textdate) == 3 &&  ($textdate[1] == 'janvier' || $textdate[1] == 'février' || $textdate[1] == 'mars'|| $textdate[1] ==  'avril'|| $textdate[1] == 'mai'|| $textdate[1] ==  'juin' 
                    || $textdate[1] == 'juillet'|| $textdate[1] == 'août'|| $textdate[1] == 'septembre'|| $textdate[1] == 'octobre'|| $textdate[1] == 'novembre'|| $textdate[1] == 'décembre')){
                        $year = $textdate[2] . "-" . $month[$textdate[1]] . "-" . $textdate[0];
                    }
                    
                    $title = str_replace("&#039;" ,"'", $title);
                    $title = str_replace("&amp;" ,"&", $title);

                    if($movie->movieExiste($title,$year)){
                        $link = ParseWeb::getMovieLinkPlatforme($title,'disney+plus');
                        sleep(2);
                        $data = $plat->addPlatform_Movie($title,$year,'disneyplus',$link);
                        if(isset($data['validation'])){
                            Logger::addLog($data['validation'],LoggerLevel::INFO,$_SERVER['REMOTE_ADDR']);
                        }else{
                            Logger::addLog($data['error'],LoggerLevel::WARNING,$_SERVER['REMOTE_ADDR']);
                        }
                    }else{
                        Logger::addLog("Le film ".$title." avec la date ".$year." n'a pas été trouvé dans la bdd" ,LoggerLevel::INFO,$_SERVER['REMOTE_ADDR']);
                    }
                }
            }
        } catch (Exception $e) {
            var_dump("OUPS l'ouverture de la page ".$page." a échoué.<br>");
        }
        return header('Location: /add/disney&page='.( $page+1 ));
    }else{
        var_dump("FIN du programe !");
    }
}

function primevideo2(int $page, Platform_MovieBusiness $plat,MovieBusiness $movie){
    var_dump("La page ".$page."<br>");
    if( $page <= 214 ){
        $month =    ['janvier' => '01' , 'février' => '02' , 'mars' => '03', 'avril' => '04', 'mai' => '05', 'juin' => '06', 
                        'juillet' => '07','août'=> '08' ,'septembre'=> '09','octobre'=> '10','novembre' => '11' ,'décembre'=>'12'];
        try {
            $url = "https://www.allocine.fr/amazon/film/tous/?page=" . $page;
            $html = new simple_html_dom();
            $html->load_file($url);
            $collection = $html->find('li[class=mdl] div[class=card entity-card entity-card-list cf]');
            foreach($collection as $element){
                $title = $element->children(1)->children(0)->children(0)->plaintext;
                if($element->children(1)->children(1)->children(1) != null){
                    $textdate = explode(' ',$element->children(1)->children(1)->children(0)->children(0)->plaintext) ;

                    if(sizeof($textdate) == 3 &&  ($textdate[1] == 'janvier' || $textdate[1] == 'février' || $textdate[1] == 'mars'|| $textdate[1] ==  'avril'|| $textdate[1] == 'mai'|| $textdate[1] ==  'juin' 
                    || $textdate[1] == 'juillet'|| $textdate[1] == 'août'|| $textdate[1] == 'septembre'|| $textdate[1] == 'octobre'|| $textdate[1] == 'novembre'|| $textdate[1] == 'décembre')){
                        $year = $textdate[2] . "-" . $month[$textdate[1]] . "-" . $textdate[0];
                    }
                    
                    $title = str_replace("&#039;" ,"'", $title);
                    $title = str_replace("&amp;" ,"&", $title);

                    if($movie->movieExiste($title,$year)){
                        $link = ParseWeb::getMovieLinkPlatforme($title,'primevideo');
                        sleep(2);
                        $data = $plat->addPlatform_Movie($title,$year,'primevideo',$link);
                        if(isset($data['validation'])){
                            Logger::addLog($data['validation'],LoggerLevel::INFO,'127.0.0.1');
                        }else{
                            Logger::addLog($data['error'],LoggerLevel::WARNING,'127.0.0.1');
                        }
                    }else{
                        Logger::addLog("Le film ".$title." avec la date ".$year." n'a pas été trouvé dans la bdd" ,LoggerLevel::INFO,'127.0.0.1');
                    }
                }
            }
        } catch (Exception $e) {
            var_dump("OUPS l'ouverture de la page ".$page." a échoué.<br>");
        }
    }else{
        var_dump("FIN du programe !");
    }
}
